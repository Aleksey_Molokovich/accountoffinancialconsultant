//
//  ReportTests.swift
//  AccountOfFinancialConsultantTests
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import XCTest
import ObjectMapper
import CoreStore

class ReportTests: XCTestCase {
    
    var items : [ReportModel] = []
    
    let dao = CoreDataDAO()
    
    override func setUp() {
        super.setUp()
        let path = Bundle.main.path(forResource: "Reports", ofType: "json")
        
        
        do {
            let fileUrl = URL(fileURLWithPath: path!)
            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: jsonData)
            items = Mapper<ReportModel>().mapArray(JSONObject: json)!
        } catch {
            
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        var reclaimed = true
        var i : Int = 0
        
        for item in items {
            dao.addOrUpdateReport(item: item) { (Succes, Err) in
                i += 1
                
                if i>=self.items.count{
                    reclaimed = false
                }
            }
        }
        
        while reclaimed {
            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
        }
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
