//
//  TasksTests.swift
//  AccountOfFinancialConsultantTests
//
//  Created by KZN-MAC-01 on 22.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import XCTest
import ObjectMapper
import CoreStore


class TasksTests: XCTestCase {
    
    var items : [TaskModel] = []
    
    let dao = CoreDataDAO()
    
    override func setUp() {
        super.setUp()
        let path = Bundle.main.path(forResource: "Tasks", ofType: "json")
        
        do {
            let fileUrl = URL(fileURLWithPath: path!)
            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: jsonData)
            items = Mapper<TaskModel>().mapArray(JSONObject: json)!
            
        } catch {
            
        }
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        var reclaimed = true
        var i : Int = 0
        
        for item in items {
            dao.addOrUpdateTask(item: item) { (Succes, Err) in
                i += 1
                
                if i>=self.items.count{
                    reclaimed = false
                }
            }
        }

        while reclaimed {
            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
        }
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
