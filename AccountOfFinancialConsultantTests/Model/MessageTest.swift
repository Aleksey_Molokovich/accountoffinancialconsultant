//
//  MessageTest.swift
//  AccountOfFinancialConsultantTests
//
//  Created by KZN-MAC-01 on 02.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import XCTest
import ObjectMapper
import CoreStore

class MessageTest: XCTestCase {
    
    var messages : [MessageModel] = []
    
    let dao = CoreDataDAO()
    
    override func setUp() {
        super.setUp()
        let path = Bundle.main.path(forResource: "Messages", ofType: "json")
        
        
        do {
            let fileUrl = URL(fileURLWithPath: path!)
            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: jsonData)
            messages = Mapper<MessageModel>().mapArray(JSONObject: json)!
            
        } catch {
            
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        var reclaimed = true
        var i : Int = 0
        
        for message in messages {
            dao.addOrUpdateMessage(item: message) { (succes, error) in
                i += 1
                
                if i>=self.messages.count{
                  reclaimed = false
                }
                
            }
        }
        
        
        while reclaimed {
            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
        }
    }
    
    func testPrintMessage(){
        CoreStore.perform(asynchronous: {transaction in
            let items = transaction.fetchAll(From<MessageCD>())
            for item in items!{
                print("uuid = \(item.uuid ?? ""), text = \(item.text ?? "")")
            }
            
            
        }, success: {_ in }, failure: {_ in })
        

    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
