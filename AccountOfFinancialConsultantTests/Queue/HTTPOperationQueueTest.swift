//
//  HTTPOperationQueueTest.swift
//  AccountOfFinancialConsultantTests
//
//  Created by Наиль  on 25.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import XCTest

class HTTPOperationQueueTest: XCTestCase {
    
    var opQueue:HTTPOperationQueue!
    
    override func setUp() {
        super.setUp()

        self.opQueue = HTTPOperationQueue.init();
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOneOperation() {
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: "https://crcal-test.sovcombank.ru/fk/confirm")!)
        request.httpMethod = "POST"
        do {
            try request.httpBody = JSONSerialization.data(withJSONObject: ["phone" : "88002000600",
                                                                           "sms" : "1234"], options: .prettyPrinted)
        } catch  {
            
        }
        
        
        
        let operation:HTTOperation = HTTOperation.init(urlRequest: request, tag: "Test");
        
//        let dict: [String:String] =
        
        
        
//        operation.setURLParamas(params: dict);
        
        var reclaimed = true;
        operation.opCompletionBlock = { [weak self] (data, sender) in
            guard self != nil else { return }
        //    sSelf.opQueue
            let responseString = String(data: data!, encoding: .utf8)
            print("responseString = \(responseString ?? "")")
            reclaimed = false;
        }
        
        self.opQueue.addOperation(operation)
        
        while reclaimed {
            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
