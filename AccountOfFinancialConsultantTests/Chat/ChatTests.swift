//
//  ChatTests.swift
//  AccountOfFinancialConsultantTests
//
//  Created by KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import XCTest
import Starscream


class ChatTests: XCTestCase, WebSocketDelegate {
    var didConnect = true
    var didDisconnect = true
    
    var didReceiveMessage = true
    
    
    func websocketDidConnect(socket: WebSocketClient) {
        let dict = [
            "os":"iOS",
            "device_token":"",
            "phone":"+79111111120",
            "cred":"Test Testovich Testov"
        ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            socket.write(data: data)
        } catch  {
            print("websocketDidConnect ERROR")
        }
        
        
        
        let dict2 = [
            "Text":"MOK test2",
            "to":"Staff",
            "status":0,
            "from": "+79111111120",
            "date":Date().timeIntervalSinceNow
            ] as [String : Any]
        
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict2, options: .prettyPrinted)
            socket.write(data: data)
        } catch  {
            print("websocketDidConnect ERROR")
        }
        
        
        didConnect = false;
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        didDisconnect = false;
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        didReceiveMessage = false;
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        didReceiveMessage = false;
    }
    
    
    var socket = WebSocket(url: URL(string: "wss://crcal.sovcombank.ru/chat/ws")!, protocols: ["chat"])
    
    override func setUp() {
        super.setUp()
        
        socket.delegate = self
        if socket.isConnected {
            disconnect()
        }
        
        socket.connect()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDidReceiveMessage() {
        
        
        while didReceiveMessage {
            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
        }
    }
    
//    func testConnect() {
//        while didConnect {
//            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
//        }
//    }
    
//    func testDisconnect() {
//        while didDisconnect {
//            RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.5))
//        }
//    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func disconnect() {
        socket.disconnect(forceTimeout: 0)
        socket.delegate = nil
    }
    
}
