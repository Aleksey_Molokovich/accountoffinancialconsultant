//
//  AppDelegate.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 13.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import CoreStore
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.configureUI()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if AccountService.isAuthorized() {
            window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "MainController")
        }else{
           window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "AuthNavigationController")
        }
        window?.makeKeyAndVisible()
        
        let storage = SQLiteStore(
            fileName: "AccountOfFinancialConsultant.xcdatamodeld",
            configuration: nil,
            migrationMappingProviders: [], // optional. The bundles that contain required .xcmappingmodel files
            localStorageOptions: .recreateStoreOnModelMismatch // optional. Provides settings that tells the DataStack how to setup the persistent store
        )
        
        do {
            try CoreStore.addStorageAndWait(storage)
            
        } catch {
            print(error)
        }
        
        Fabric.with([Crashlytics.self])
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
//        self.saveContext()
    }

}

