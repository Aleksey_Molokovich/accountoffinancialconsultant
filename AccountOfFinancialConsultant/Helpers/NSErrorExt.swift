//
//  NSErrorExt.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 05.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

extension NSError {
    /// Rounds the double to decimal places value
    func statusCode() -> Int {
        let userInfo = self.userInfo
        
        var response : HTTPURLResponse!
        guard userInfo["HTTPOperationErrorHTTPResponseKey"] != nil else{
            return -1
        }
        
        response = (userInfo["HTTPOperationErrorHTTPResponseKey"] as! HTTPURLResponse)
        
        return response!.statusCode
    }
    
    func statusCodeDescription() -> String? {
        
        switch self.statusCode() {
        case -1 :
            return "Нет подключения к интернету"
        case 500... :
            return "Сервер не отвечает"
        case 422 :
            return "Неверный код"
        default:
            return nil
        }
    }
}
