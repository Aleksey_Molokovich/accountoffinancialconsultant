//
//  UIImageExt.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 06.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}
