//
//  UIColorHelper.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

enum MOCColor {
    case Blue, Green, Red, DarkBlue, Gray, GridLine
    
    func toUIColor() -> UIColor {
        switch self {
        case .Blue:
            return UIColor(hexString: "1E8BC3")
        case .Green:
            return UIColor(hexString: "03A678")
        case .Red:
            return UIColor(hexString: "D24D57")
        case .DarkBlue:
            return UIColor(hexString: "22313F")
        case .Gray:
            return UIColor(hexString: "98A4A9")
        case .GridLine:
            return UIColor(hexString: "C5CBCC")
        }
    }
}

extension UIColor {

    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    
    

}
