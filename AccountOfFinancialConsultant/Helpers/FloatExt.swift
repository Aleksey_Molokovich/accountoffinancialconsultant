//
//  DoubleExt.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

extension Float {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
