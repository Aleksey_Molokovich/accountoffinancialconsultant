//
//  UIViewExt.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 06.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//


extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}
