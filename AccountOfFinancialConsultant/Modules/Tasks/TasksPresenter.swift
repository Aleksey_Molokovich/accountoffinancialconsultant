//
//  TasksPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TasksPresenter: TasksPresenterProtocol, TasksInteractorOutputProtocol, TaskDetailOutputModuleProtocol {


    weak private var view: TasksViewProtocol?
    var interactor: TasksInteractorInputProtocol?{
        didSet{
            interactor?.load()
        }
    }
    var router: TasksRouterProtocol!
    
    var selectedTask : TaskModel?
    
    var openedTasksVisable = true

    init(view: TasksViewProtocol) {
        self.view = view
    }

    func configureView() {
    }
    
    func openedTasks(show: Bool) {
        openedTasksVisable = show
        interactor?.readFromStore()
    }
    
    func selectedTask(task : TaskModel) {
        selectedTask = task
        router.pushTaskDetailController()
    }
    
    
    func viewWillAppear(){
        interactor?.readFromStore()
        interactor?.load()
    }
    // MARK: - InteractorOutputProtocol
    
    func update(tasks: [TaskModel]) {

        
        let status = openedTasksVisable ? 0 : 1
        
        let filtered = tasks.filter({ $0.status == status})
        
        let sorted = sortTask(tasks: filtered)
        
        view?.update(tasks: sorted)
    }
    
    func fail(error: String) {
        
    }
    // MARK: - OutputModuleProtocol
    
    func apllySelectedTask() -> TaskModel {
        return selectedTask!
    }
    
    func sortTask(tasks : [TaskModel]) -> [TaskModel] {
        
        if Services.shared.taskService.sortByDate && !Services.shared.taskService.sortByPriority{
            return tasks.sorted(by: { $0.createdAt?.compare($1.createdAt!) == .orderedDescending })
        }else if !Services.shared.taskService.sortByDate && Services.shared.taskService.sortByPriority{
            return tasks.sorted(by: {$0.priority < $1.priority})
        }else{
            return tasks.sorted(by: {
                if $0.priority != $1.priority { // first, compare by last names
                    return $0.priority < $1.priority
                }else{
                    return $0.createdAt?.compare($1.createdAt!) == .orderedDescending
                }
            })
        }
    }
}
