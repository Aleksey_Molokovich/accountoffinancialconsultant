//
//  TasksViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TasksViewController: UIViewController, TasksViewProtocol, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blankView: BlankView!

    @IBOutlet weak var segmentedControl: AFCSegmentControl!
    
    let cellReuseIdentifier = "TaskTableViewCell"
    
	var presenter: TasksPresenterProtocol!

    let configurator: TasksConfiguratorProtocol = TasksConfigurator()

    var data : [TaskModel]?
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        blankView.isHidden = true
        configurator.configure(with: self)
        presenter.configureView()
        createSegmentedControl()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    func createSegmentedControl() {
        segmentedControl.backgroundColor = .white
        segmentedControl.commaSeperatedButtonTitles = "Открытые,Закрытые"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(segment:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
        
    }
    
    // MARK: - Action methods
    
    @objc func onChangeOfSegment(segment :AFCSegmentControl ) {
        if segment.selectedSegmentIndex == 0 {
            presenter.openedTasks(show : true)
        }else{
            presenter.openedTasks(show : false)
        }
    }
    
     // MARK: - TableView methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data == nil {
            return 0
        }
        return (data?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TaskTableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TaskTableViewCell?)!
        
        cell.configure(withTask: data![indexPath.row])
        
        return cell
    }	
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        presenter.selectedTask(task : data![indexPath.row])
    }

    // MARK: - AboutViewProtocol methods

    
    func update(tasks: [TaskModel]) {
        data = tasks
        blankView.isHidden = !tasks.isEmpty
        if segmentedControl.selectedSegmentIndex == 0 {
            blankView.blankType = .noOpenTasks
        }else{
            blankView.blankType = .noClosedTasks
        }
        let offset = tableView.contentOffset
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
        self.tableView.contentOffset = offset
    }
}
