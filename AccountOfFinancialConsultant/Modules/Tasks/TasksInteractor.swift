//
//  TasksInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import ObjectMapper

class TasksInteractor: TasksInteractorInputProtocol {
    
    var operation:Operation?

    var lastUpdate : Date?
    
    weak var presenter: TasksInteractorOutputProtocol?
    
    required init(presenter: TasksInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    
    func load() {
        #if DEBUGMOCK
            readFromFile()
            return
        #endif
        
        if operation != nil && (operation?.isExecuting)! { return }
        
        if lastUpdate != nil && Date().timeIntervalSince(lastUpdate!) < 60 {
            return
        }
        
        operation = API.tasks(completion: {[weak self] (json) in
            guard let sSelf = self else { return }
            let tasks = Mapper<TaskModel>().mapArray(JSONObject: json)!
            sSelf.updateTasksInStore(tasks: tasks)
            sSelf.lastUpdate = Date()
        }) {[weak self] (error) in
            guard let sSelf = self else { return }
            sSelf.presenter?.fail(error: "")
        }
        
        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
    }
    
    
    func updateTasksInStore(tasks : [TaskModel]) {
        let dao = Services.shared.dao
        
        var i : Int = 0
        for task in tasks {
            dao.addOrUpdateTask(item: task) { [weak self] (Succes, Err) in
                guard let strongSelf = self else { return }
                
                i += 1
                if i == tasks.count {
                    strongSelf.presenter?.update( tasks: tasks)
                }
            }
        }
    }

    
    func readFromStore()  {
        
        let dao = Services.shared.dao
        
        dao.tasks {[weak self] (tasks, Err) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.presenter?.update( tasks: tasks!)
        }
    }
    
    
    func readFromFile() {

        let path = Bundle.main.path(forResource: "Tasks", ofType: "json")

        do {
            let fileUrl = URL(fileURLWithPath: path!)
            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try! JSONSerialization.jsonObject(with: jsonData)
            let tasks = Mapper<TaskModel>().mapArray(JSONObject: json)!
            self.updateTasksInStore(tasks:tasks)
        } catch {

        }
    }
}
