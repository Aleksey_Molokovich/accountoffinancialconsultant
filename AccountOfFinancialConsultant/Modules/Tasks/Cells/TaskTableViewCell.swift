//
//  TaskTableViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var descriptionTaskLabel: UILabel!
    
    
    @IBOutlet weak var statusView: StatusView!
    override func awakeFromNib() {
        super.awakeFromNib()
        statusView.align = .left
        
    }

    func configure(withTask task:TaskModel) {
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "d MMMM"
        
        dateLabel.text = dateFormatterPrint.string(from: task.createdAt!)
        
        descriptionTaskLabel.text = task.name
        descriptionTaskLabel.textColor = MOCColor.DarkBlue.toUIColor()
        
        switch task.priority {
        case 1:
            statusView.status = StatusTaskEnum.high
        case 2:
            statusView.status = StatusTaskEnum.middle
        case 3:
            statusView.status = StatusTaskEnum.low
        default:
            break
        }


    }

}
