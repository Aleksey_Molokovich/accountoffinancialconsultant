//
//  TasksProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol TasksConfiguratorProtocol: class {
    func configure(with viewController: TasksViewProtocol)
}

//MARK: Router -
protocol TasksRouterProtocol: class {
    func pushTaskDetailController()
}
//MARK: Presenter -
protocol TasksPresenterProtocol: class {

    var interactor: TasksInteractorInputProtocol? { get set }
    var router: TasksRouterProtocol! { set get }
    func configureView()
    func viewWillAppear()
    func openedTasks(show : Bool)
    
    func selectedTask(task : TaskModel)
}

//MARK: Interactor -
protocol TasksInteractorOutputProtocol: class {
    func update(tasks : [TaskModel])
    func fail(error : String)
    /* Interactor -> Presenter */
}

protocol TasksInteractorInputProtocol: class {

    var presenter: TasksInteractorOutputProtocol?  { get set }
    func readFromStore()
    func load()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol TasksViewProtocol: class {

    var presenter: TasksPresenterProtocol!  { get set }
    func update(tasks : [TaskModel])
    /* Presenter -> ViewController */
}
