//
//  TasksRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TasksRouter: TasksRouterProtocol {

    weak var viewController: UIViewController?

    init(viewController: TasksViewProtocol) {
        self.viewController = (viewController as! UIViewController)
    }
    
    func pushTaskDetailController() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewProtocol
        
        nextViewController.configure(delegate: (viewController as! TasksViewProtocol).presenter as! TaskDetailOutputModuleProtocol)
        
        viewController?.navigationController?.pushViewController(nextViewController as! UIViewController, animated: true)
        
    }
}
