//
//  TasksConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TasksConfigurator: TasksConfiguratorProtocol {

    func configure(with viewController: TasksViewProtocol) {
        let presenter = TasksPresenter(view: viewController)
        let interactor = TasksInteractor(presenter: presenter)
        let router = TasksRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
