//
//  TaskFilterViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TaskFilterViewController: UIViewController {

    
    @IBOutlet weak var prioritySwitcher: UIButton!
    @IBOutlet weak var priorityCheker: UIImageView!
    
    @IBOutlet weak var dateSwitcher: UIButton!
    @IBOutlet weak var dateCheker: UIImageView!
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        prioritySwitcher.tag = Services.shared.taskService.sortByPriority ? 1 : 0
        dateSwitcher.tag = Services.shared.taskService.sortByDate ? 1 : 0
        showChecker(priority : Services.shared.taskService.sortByPriority,
                    date : Services.shared.taskService.sortByDate)
    }
    
    
    // MARK: - Action methods
    
    @IBAction func switchByPriority(_ sender: Any) {
        prioritySwitcher.tag = prioritySwitcher.tag == 0 ? 1 : 0
        
        if prioritySwitcher.tag == 0 && dateSwitcher.tag == 0{
            prioritySwitcher.tag = 1
        }
        Services.shared.taskService.sortByPriority = prioritySwitcher.tag == 1 ? true : false
        Services.shared.taskService.sortByDate = dateSwitcher.tag == 1 ? true : false
        showChecker(priority : Services.shared.taskService.sortByPriority,
                    date : Services.shared.taskService.sortByDate)
    }
    
    @IBAction func switchByDate(_ sender: Any) {
        dateSwitcher.tag = dateSwitcher.tag == 0 ? 1 : 0
        
        if prioritySwitcher.tag == 0 && dateSwitcher.tag == 0 {
            dateSwitcher.tag = 1
        }
        Services.shared.taskService.sortByPriority = prioritySwitcher.tag == 1 ? true : false
        Services.shared.taskService.sortByDate = dateSwitcher.tag == 1 ? true : false
        showChecker(priority : Services.shared.taskService.sortByPriority,
                    date : Services.shared.taskService.sortByDate)
    }
    
    func showChecker(priority : Bool, date : Bool)  {
        self.dateCheker.isHidden = !date
        self.priorityCheker.isHidden = !priority
    }
}
