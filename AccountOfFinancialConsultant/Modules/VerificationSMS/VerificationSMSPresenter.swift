//
//  VerificationSMSPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class VerificationSMSPresenter: VerificationSMSPresenterProtocol, VerificationSMSInteractorOutputProtocol {

    weak private var view: VerificationSMSViewProtocol?
    var interactor: VerificationSMSInteractorInputProtocol?
    var router: VerificationSMSRouterProtocol!

    init(view: VerificationSMSViewProtocol) {
        self.view = view
    }

    func configureView() {
        
    }
    
    func sendNewCode() {
        interactor?.sendNewCode()
    }
    
    func showAbout() {
        router.pushAboutController()
    }
    
    func verifySMSCode(code: String) {
        DispatchQueue.main.async {[unowned self] in
            self.view?.showActivity()
            self.interactor?.smsCodeVerifyRequest(code: code)
        }
    }
    
    func succesful() {
         DispatchQueue.main.async {[unowned self] in
            self.view?.hideActivity()
            self.router.presentMainController()
        }
    }

    
    func fail(error: String?, code : Int) {
         DispatchQueue.main.async {[unowned self] in
            self.view?.hideActivity()
            if code == 422 {
                self.view?.changeStateCodeView(wrong: true)
            }
            guard error != nil else {return}
            self.router.presentAlert(text: error!)
        }
    }
}
