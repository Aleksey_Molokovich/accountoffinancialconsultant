//
//  VerificationSMSRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit


class VerificationSMSRouter: VerificationSMSRouterProtocol {
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

    weak var viewController: UIViewController?

    init(viewController: VerificationSMSViewProtocol) {
        self.viewController = viewController as? UIViewController
    }
    
    
    
    func pushAboutController() {
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AboutViewController") as UIViewController
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }

    func presentMainController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = storyBoard.instantiateViewController(withIdentifier: "MainController")
    }
    
    func presentAlert(text : String){
        let alert = UIAlertController(title: "Ошибка", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: nil))
        self.viewController?.present(alert, animated: true, completion: nil)
    }
}
