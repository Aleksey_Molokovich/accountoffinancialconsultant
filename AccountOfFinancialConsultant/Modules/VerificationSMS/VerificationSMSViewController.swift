//
//  VerificationSMSViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class VerificationSMSViewController: UIViewController, VerificationSMSViewProtocol, UITextFieldDelegate {

    @IBOutlet weak var entryButton: BigBlueButton!
    var presenter: VerificationSMSPresenterProtocol!

    @IBOutlet weak var textContainerView: RoundedView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var smsCodeTextField: UITextField!
    
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    let configurator: VerificationSMSConfiguratorProtocol = VerificationSMSConfigurator()
    var observation: NSKeyValueObservation!
    
    var seconds = 30
    var timer = Timer()
    
    var isTimerRunning = false
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        repeatButton.isHidden = true
        durationLabel.isHidden = false
        configurator.configure(with: self)
        presenter.configureView()
        textContainerView.layer.borderWidth = 1
        textContainerView.layer.borderColor = UIColor.lightGray.cgColor 
        smsCodeTextField.delegate = self
        durationLabel.text = "Повторная отправка SMS возможна через 00:30"
        #if DEBUG
//        smsCodeTextField.text = "1234"
        #endif
        self.entryButton.isEnabled = false

//        observation = codeTextField.observe(\.text){ (codeTextField, change) in
//
//            if codeTextField.text?.count == 4
//            {
//                self.entryButton.isEnabled = true
//            }else{
//                self.entryButton.isEnabled = false
//            }
//        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            //            let endFrameY = endFrame?.origin.y ??
            
            self.bottomConstraint.constant = (endFrame?.size.height)! - 30
            UIView.animate(withDuration: 0,
                           delay:0,
                           options: .curveEaseInOut,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        self.bottomConstraint.constant = 70
        UIView.animate(withDuration: 0,
                       delay:0,
                       options: .curveEaseInOut,
                       animations: { self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        runTimer()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            repeatButton.isHidden = false
            durationLabel.isHidden = true
        } else {
            seconds -= 1
            durationLabel.text = "Повторная отправка SMS возможна через " + timeString(time: TimeInterval(seconds))
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    // MARK: - Action methods
    
    
    @IBAction func pressSendCode(_ sender: Any) {
        presenter.sendNewCode()
        seconds = 30
        repeatButton.isHidden = true
        durationLabel.isHidden = false
        durationLabel.text = "Повторная отправка SMS возможна через 00:30"
        runTimer()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func entryAction(_ sender: Any) {
        presenter.verifySMSCode(code: smsCodeTextField.text!)
    }
    
    
    @IBAction func clickAbout(_ sender: Any) {
        presenter.showAbout()
    }
    
    
    // MARK: - TextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let filter = string.filter { "0123456789".contains($0) }
        
        if filter.count == 0 && string.count != 0 {
            return false
        }
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let code = text.replacingCharacters(in: textRange,
                                                       with: string)
            if code.count > 4 {
                return false
            }
            
            if code.count == 4
            {
                self.entryButton.isEnabled = true
            }else{
                self.entryButton.isEnabled = false
            }
            
        }
        changeStateCodeView(wrong: false)
        return true
    }
    
    
    // MARK: - ViewProtocol methods
    
    func changeStateCodeView(wrong : Bool){
        if wrong {
            textContainerView.layer.borderColor = UIColor.red.cgColor
            smsCodeTextField.textColor = UIColor.red
        }else{
            textContainerView.layer.borderColor = UIColor.init(hexString: "#E0E7E8").cgColor
            smsCodeTextField.textColor = UIColor.black
        }
    }
    
    func showActivity(){
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivity(){
        activityIndicator.stopAnimating()
    }
}
