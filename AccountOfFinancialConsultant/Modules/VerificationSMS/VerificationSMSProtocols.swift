//
//  VerificationSMSProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol VerificationSMSConfiguratorProtocol: class {
    func configure(with viewController: VerificationSMSViewProtocol)
}

//MARK: Router -
protocol VerificationSMSRouterProtocol: class {
    func presentAlert(text : String)
    func presentMainController()
    func pushAboutController()
}
//MARK: Presenter -
protocol VerificationSMSPresenterProtocol: class {

    var interactor: VerificationSMSInteractorInputProtocol? { get set }
    var router: VerificationSMSRouterProtocol! { set get }
    func configureView()
    func verifySMSCode(code : String)
    func showAbout()
    func sendNewCode()
}

//MARK: Interactor -
protocol VerificationSMSInteractorOutputProtocol: class {
    func succesful()
    func fail(error : String?, code : Int)
    /* Interactor -> Presenter */
}

protocol VerificationSMSInteractorInputProtocol: class {

    var presenter: VerificationSMSInteractorOutputProtocol?  { get set }
    func smsCodeVerifyRequest(code : String)
    func sendNewCode()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol VerificationSMSViewProtocol: class {

    var presenter: VerificationSMSPresenterProtocol!  { get set }
    func changeStateCodeView(wrong : Bool)
    func showActivity()
    func hideActivity()
    /* Presenter -> ViewController */
}
