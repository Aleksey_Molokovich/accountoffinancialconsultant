//
//  VerificationSMSInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import ObjectMapper

class VerificationSMSInteractor: VerificationSMSInteractorInputProtocol {

    weak var presenter: VerificationSMSInteractorOutputProtocol?
    
    var operation:Operation?
    
    required init(presenter: VerificationSMSInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func smsCodeVerifyRequest(code : String) {
        
        operation = API.verifyCode(code: code, completion: {[weak self] (json) in
            guard let sSelf = self else { return }
            
            let authModel = Mapper<AuthModel>().map(JSON: json as! [String : Any])!
            
            let fio = authModel.surname + " " + authModel.name + " " + authModel.lastname
            let phone = Services.shared.accountService.phone
            
            Services.shared.accountService.succesfullAuthorized(login: phone, fio: fio, uuid: authModel.uuid)
            
            sSelf.presenter?.succesful()
        }) {[unowned self] (error) in
            self.presenter?.fail(error: error.statusCodeDescription(), code : error.statusCode() )
        }
        
        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
        
    }
    
    func sendNewCode() {
        
        let phone = Services.shared.accountService.phone
        
        operation = API.sendSMSTo(phone: phone, completion: { (json) in

            
        }) { (error) in
            DispatchQueue.main.async {[weak self] in
                guard let sSelf = self else { return }
                sSelf.presenter?.fail(error: "", code : -1)
            }
        }
        
        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
    }
}
