//
//  VerificationSMSConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class VerificationSMSConfigurator: VerificationSMSConfiguratorProtocol {

    func configure(with viewController: VerificationSMSViewProtocol) {
        let presenter = VerificationSMSPresenter(view: viewController)
        let interactor = VerificationSMSInteractor(presenter: presenter)
        let router = VerificationSMSRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
