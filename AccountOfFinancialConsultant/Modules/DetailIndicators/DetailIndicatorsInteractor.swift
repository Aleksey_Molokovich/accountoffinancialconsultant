//
//  DetailIndicatorsInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class DetailIndicatorsInteractor: DetailIndicatorsInteractorInputProtocol {

    weak var presenter: DetailIndicatorsInteractorOutputProtocol?
    
    required init(presenter: DetailIndicatorsInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func readStore()  {
        
        let dao = Services.shared.dao
        
        dao.reports {[weak self] (items, Err) in
            guard let strongSelf = self else { return }
            
            guard let _ = items else{ return }
            
            strongSelf.presenter?.update(reports:items!)
        }
    }
}
