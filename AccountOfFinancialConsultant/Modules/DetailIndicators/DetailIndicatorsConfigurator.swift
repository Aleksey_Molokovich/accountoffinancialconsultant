//
//  DetailIndicatorsConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class DetailIndicatorsConfigurator: DetailIndicatorsConfiguratorProtocol {

    func configure(with viewController: DetailIndicatorsViewProtocol) {
        let presenter = DetailIndicatorsPresenter(view: viewController)
        let interactor = DetailIndicatorsInteractor(presenter: presenter)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
