//
//  DetailIndicatorsViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Charts

class DetailIndicatorsViewController: UIViewController, DetailIndicatorsViewProtocol, ChartViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var chartView: LineChartView!
    
    @IBOutlet weak var legendView: UIStackView!
    @IBOutlet weak var segmentedControl: AFCSegmentControl!
    
    @IBOutlet weak var blankView: BlankView!
    
    @IBOutlet weak var shiftRightButton: UIButton!
    @IBOutlet weak var shiftLeftButton: UIButton!
    
    var presenter: DetailIndicatorsPresenterProtocol!

    let configurator: DetailIndicatorsConfiguratorProtocol = DetailIndicatorsConfigurator()

    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
        blankView.isHidden = true
        blankView.blankType = .noChartsData
        chartView.delegate = self
        configureChartView()
        createSegmentedControl()
    }
    
    func createSegmentedControl() {
        segmentedControl.backgroundColor = .white
        segmentedControl.commaSeperatedButtonTitles = "Еженедельный,Ежемесячный"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(control:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 1
        
        shiftLeftButton.isHidden = true
        shiftRightButton.isHidden = true
        
    }
    
    func configureChartView(){
        chartView.fitScreen()

        let pan = UIPanGestureRecognizer(target: self, action: #selector(moveChart(gestureRecognizer:)))
        pan.delegate = self
        chartView.addGestureRecognizer(pan)
        
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.highlightPerTapEnabled = true
        chartView.highlightPerDragEnabled = false
        chartView.extraTopOffset = 30
        chartView.extraRightOffset = 10
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        
        chartView.maxVisibleCount = 6
        
        let l = chartView.legend
        l.enabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
        xAxis.labelTextColor = UIColor(hexString: "#98A4A9")
        xAxis.axisLineColor = MOCColor.GridLine.toUIColor()
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.granularityEnabled = true
        
        let leftAxis = chartView.leftAxis
        leftAxis.enabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.drawZeroLineEnabled = false
        leftAxis.labelTextColor = UIColor(hexString: "#98A4A9")
        
        leftAxis.axisLineColor = MOCColor.GridLine.toUIColor()
        leftAxis.drawAxisLineEnabled = false
        leftAxis.gridLineCap = .butt
        leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
        leftAxis.gridColor = MOCColor.GridLine.toUIColor()
                leftAxis.axisMinimum = 0
        leftAxis.granularityEnabled = false
        leftAxis.xOffset = 10
         leftAxis.valueFormatter = PersentAbsAxisValueFormatter()
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        
    }
    
    // MARK: - Action methods
    
    @objc func moveChart(gestureRecognizer: UIPanGestureRecognizer){
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            let velocity = gestureRecognizer.velocity(in: chartView).x

            let mod =  abs(Int(translation.x)) % 15
            if  mod <= 1 && velocity < 0 {
                presenter.moveChart(right: true)
            }
            
            if  mod <= 1 && velocity > 0 {
                presenter.moveChart(right: false)
            }
            chartView.highlightValue(x: 0, dataSetIndex: 100)
            
        }
    }
    
    @objc func onChangeOfSegment(control :AFCSegmentControl ) {
        let period = control.selectedSegmentIndex == 0 ? true : false
        
        
        shiftLeftButton.isHidden = !period
        shiftRightButton.isHidden = !period
        
        presenter.switchPeriod(toWeek : period)
        chartView.highlightValue(x: 0, dataSetIndex: 100)
    }

    
    @IBAction func pressMoveLeft(_ sender: Any) {
        presenter.moveChart(right: false)
        chartView.highlightValue(x: 0, dataSetIndex: 100)
        
    }
    
    @IBAction func pressMoveRight(_ sender: Any) {
        presenter.moveChart(right: true)
        chartView.highlightValue(x: 0, dataSetIndex: 100)
    }
    
    
    // MARK: - ViewProtocol methods
    
    func disableMoveBtn(left: Bool, right: Bool) {
        shiftLeftButton.isEnabled = right
        shiftRightButton.isEnabled = left
    }
    
    func updateChartData(data : LineChartData?) {
        
        guard let chartData = data else {
            chartView.clear()
            legendView.isHidden = true
            blankView.isHidden = false
            return
        }
        blankView.isHidden = true
        legendView.isHidden = false
        
        chartView.marker = LineChartMarker()
        
        let xAxis = chartView.xAxis
        xAxis.labelCount = presenter.titles().count
        
        var maxEndPoint = 5
        if maxEndPoint >= presenter.titles().count{
            maxEndPoint = presenter.titles().count - 1
        }
        
        xAxis.axisMinimum = chartData.xMin - 0.3
        xAxis.axisMaximum = chartData.xMin + Double(maxEndPoint) + 0.3
        xAxis.valueFormatter = CustomTitlesAxisValueFormatter(titles: presenter.titles())
        chartView.data = chartData

    }
    
    func updateChartData(data: LineChartData, minX: Double) {
        let xAxis = chartView.xAxis
        xAxis.axisMinimum = minX - 0.3
        xAxis.axisMaximum = minX + 5 + 0.3

        chartView.data = data
    }
}
