//
//  DetailIndicatorsPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Charts

class DetailIndicatorsPresenter: DetailIndicatorsPresenterProtocol, DetailIndicatorsInteractorOutputProtocol {
   
    var xAxisTitles : [String] = []
    var isWeek = false
    var data : LineChartData?
    var minX : Double = 0
    
    weak private var view: DetailIndicatorsViewProtocol?
    var interactor: DetailIndicatorsInteractorInputProtocol?

    init(view: DetailIndicatorsViewProtocol) {
        self.view = view
    }

    func configureView() {
        interactor?.readStore()
    }
    
    func switchPeriod(toWeek: Bool) {
        isWeek = toWeek
        interactor?.readStore()
    }
    
    func titles() -> [String] {
        return xAxisTitles
    }
    
    func moveChart(right: Bool) {
        if !isWeek {
            return
        }
        
        if minX < 0 {
            minX = 0
            view?.disableMoveBtn(left: true, right: false)
            return
        }else if minX > Double(xAxisTitles.count - 6) {
            minX = Double(xAxisTitles.count - 6)
            view?.disableMoveBtn(left: false, right: true)
            return
        }else{
            view?.disableMoveBtn(left: true, right: true)
        }
        
        if right {
            self.minX += 1
            view?.updateChartData(data: self.data!, minX: self.minX)
        }else{
            self.minX -= 1
            view?.updateChartData(data: self.data!, minX: self.minX)
        }
                
    }
    //MARK: InteractorOutputProtocol -
    
    func update(reports: [ReportModel]?) {
        
        guard let filtered = reports?.filter({$0.period == (self.isWeek ? "week" : "month")}), filtered.count > 0  else {
            view?.updateChartData(data: nil)
            return
        }
        
        let sorted = filtered.sorted(by: { $0.periodDate?.compare($1.periodDate!) == .orderedDescending })

        self.xAxisTitles = []
        
        var ranged = sorted
        
        let maxCount = isWeek ? 11 : 5
        
        guard let firstIndexNotEmptyFPD = sorted.index(where: { $0.fpd >= 0 }) else {
            view?.updateChartData(data: nil)
            return
        }
        
        var firstSixNotEmptyFPD : [ReportModel] = []
        for (index,report) in sorted[firstIndexNotEmptyFPD...].enumerated() {
            if index > maxCount { break}
            firstSixNotEmptyFPD.append(report)
        }
        
        ranged = (firstSixNotEmptyFPD.sorted(by: { $1.periodDate?.compare($0.periodDate!) == .orderedDescending }))
        
        let setFPD = genDataSet(color: .Blue)
        
        let setSPD = genDataSet(color: .Red)
        
        let setTPD = genDataSet(color: .Green)
        
        var allDataSet : [LineChartDataSet] = [setFPD, setSPD, setTPD]
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = isWeek ? "d MMM" : "MMM"

        
        // если есть пропуск в данных, то он создает новый сет и строит новую кривую
        for (index, rep) in ranged.enumerated() {
            let monthIndex = Calendar.current.component(.month, from: rep.periodDate!)
            let months = ["янв","фев","мар","апр","май","июн","июл","авг","сен","окт","ноя","дек"]
            let month = months[monthIndex - 1]
            if isWeek {
                dateFormatterPrint.dateFormat = "d"
                let day = dateFormatterPrint.string(from: rep.periodDate!)
                self.xAxisTitles.append(day + " " + month )
            }else{
                self.xAxisTitles.append(month)
            }
            
            
            let fpd = allDataSet.filter({$0.colors.first == MOCColor.Blue.toUIColor()}).last
            if rep.fpd > 0{
                fpd?.values.append(ChartDataEntry(x: Double(index), y: Double(rep.fpd)*100))
            }else if (fpd?.entryCount)! > 0{
                allDataSet.append(genDataSet(color: .Blue))
            }
            
            let spd = allDataSet.filter({$0.colors.first == MOCColor.Red.toUIColor()}).last
            if rep.spd > 0{
                spd?.values.append(ChartDataEntry(x: Double(index), y: Double(rep.spd)*100))
            }else if (spd?.entryCount)! > 0{
                allDataSet.append(genDataSet(color: .Red))
            }
            
            let tpd = allDataSet.filter({$0.colors.first == MOCColor.Green.toUIColor()}).last
            if rep.tpd > 0{
                tpd?.values.append(ChartDataEntry(x: Double(index), y: Double(rep.tpd)*100))
            }else if (tpd?.entryCount)! > 0{
                allDataSet.append(genDataSet(color: .Green))
            }
        }
        
        data = LineChartData(dataSets: allDataSet)
        data?.setDrawValues(false)
        
        view?.updateChartData(data: data!)
    }
    
    func genDataSet(color : MOCColor) -> LineChartDataSet {
        let set = LineChartDataSet(values: [], label: "")
        set.setColor(color.toUIColor())
        set.setCircleColor(color.toUIColor())
        set.lineWidth = 3
        set.mode = .horizontalBezier
        set.circleRadius = 5
        set.setDrawHighlightIndicators(false)
        
        set.drawCircleHoleEnabled = false
        
        return set
    }
}
