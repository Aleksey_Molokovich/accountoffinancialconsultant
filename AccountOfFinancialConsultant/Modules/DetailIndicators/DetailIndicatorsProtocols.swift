//
//  DetailIndicatorsProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
import Charts
//MARK: Configurator -
protocol DetailIndicatorsConfiguratorProtocol: class {
    func configure(with viewController: DetailIndicatorsViewProtocol)
}


//MARK: Presenter -
protocol DetailIndicatorsPresenterProtocol: class {

    var interactor: DetailIndicatorsInteractorInputProtocol? { get set }
    func configureView()
    func titles() -> [String]
    func switchPeriod(toWeek:Bool)
    func moveChart(right : Bool)
}

//MARK: Interactor -
protocol DetailIndicatorsInteractorOutputProtocol: class {
    func update(reports : [ReportModel]?)
    /* Interactor -> Presenter */
}

protocol DetailIndicatorsInteractorInputProtocol: class {

    var presenter: DetailIndicatorsInteractorOutputProtocol?  { get set }
    func readStore()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol DetailIndicatorsViewProtocol: class {

    var presenter: DetailIndicatorsPresenterProtocol!  { get set }
    func updateChartData(data : LineChartData?)
    func updateChartData(data : LineChartData, minX : Double )
    func disableMoveBtn(left : Bool, right: Bool)
    /* Presenter -> ViewController */
}
