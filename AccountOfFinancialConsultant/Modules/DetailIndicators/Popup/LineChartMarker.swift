//
//  LineChartMarker.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

class LineChartMarker: IMarker {
    
    var view : ChartMarkerViewProtocol? = nil
    
    
    var offset: CGPoint = CGPoint(x: 0, y: 0 )
    
    func offsetForDrawing(atPoint: CGPoint) -> CGPoint {
        return atPoint
    }
    
    func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        let value :ChartDataEntry = entry
        
        view = SinglePersentValuePopupView.init(values: [value.y])
        
    }
    
    func draw(context: CGContext, point: CGPoint) {
        
        let scale = UIScreen.main.scale
        
        let widthCtx = CGFloat(context.width)/scale
        let widthPopup = (view as! UIView).frame.width
        
        var newPoint = point
        newPoint.x = point.x - widthPopup/2.0
        newPoint.y = point.y - ((view as! UIView).frame.height) - 10
        
        var pading = Double(widthCtx) - Double(point.x+widthPopup/2.0 - 5)
        if  pading < 0{
            newPoint.x = newPoint.x + CGFloat(pading)
        }else{
            pading = 0
        }
        
        print(point, newPoint)
        guard let image = view?.imageView(pading: CGFloat(pading), pointDown: true) else { return }
        image.draw(at: newPoint)
        UIGraphicsEndImageContext()
        
    }
}

