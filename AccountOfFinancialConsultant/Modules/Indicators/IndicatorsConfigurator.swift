//
//  IndicatorsConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class IndicatorsConfigurator: IndicatorsConfiguratorProtocol {

    func configure(with viewController: IndicatorsViewProtocol) {
        let presenter = IndicatorsPresenter(view: viewController)
        let interactor = IndicatorsInteractor(presenter: presenter)
        let router = IndicatorsRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
