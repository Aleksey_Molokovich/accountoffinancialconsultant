//
//  IndicatorsPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Charts



class IndicatorsPresenter: IndicatorsPresenterProtocol, IndicatorsInteractorOutputProtocol, BaseCellProtocol {
    
    weak private var view: IndicatorsViewProtocol?
    var interactor: IndicatorsInteractorInputProtocol?
    var router: IndicatorsRouterProtocol!

    var reports: [ReportModel] = []
    var dateReport : Date?
    
    let dateFormatterPrint = DateFormatter()
    
    init(view: IndicatorsViewProtocol) {
        self.view = view
    }

    func configureView() {
        interactor?.load()
        interactor?.readStore()
        view?.navigation(title: Services.shared.accountService.userFIO)
    }
    
    func viewWillAppear(){
        interactor?.load()
        interactor?.readStore()
    }
    func cell(cellIindex: Int) -> String {
        switch cellIindex {
        case 0:
            return "ClientsPieChartTableViewCell"
        case 1:
            return "IndicatorsBarChartTableViewCell"
        case 2:
            return "RejectTableViewCell"
        default:
            return ""
        }
    }
    
    func showRejectChartDetail() {
        router.pushChartDetailController(type: .reject)
    }
    
    func showIndicatorsChartDetail() {
        router.pushChartDetailController(type: .indicators)
    }
    
    func showClientChartDetail() {
        router.pushChartDetailController(type: .clients)
    }
    //MARK: InteractorOutputProtocol
    
    
    func update(reports: [ReportModel]?) {
        guard let _ = reports  else { return }
        
        let filtered = reports?.filter({$0.period == "month"})
        
        self.reports = (filtered?.sorted(by: { $0.periodDate! > $1.periodDate! }))!
        dateReport = reports?.first?.periodDate
        view?.update()
    }
    
    func fail(error: String) {
        
    }
    
    //MARK: BaseCellProtocol
    
    func reportDate() -> String? {
        dateFormatterPrint.dateFormat = "MMMM yyyy"
        guard let _ = dateReport else { return nil }
        return dateFormatterPrint.string(from: dateReport!)
    }
    
    func clientsPieChartData() -> [ChartData]? {
        guard !reports.isEmpty else{
            
            return [emptyPieData(date: Date())]
            
        }
        
        var dataCollection :[ChartData] = []
        
        for (index, report) in reports.enumerated() {
            
            if index > 5 { break }
            
            if report.low_450 == 0 &&
                report.from_450_to_650 == 0 &&
                report.over_650 == 0 {
                dataCollection.append(emptyPieData(date: report.periodDate!))
                continue
            }
            
            var entries : [PieChartDataEntry] = []
            let amount = report.low_450 + report.from_450_to_650 + report.over_650
            
            entries.append(PieChartDataEntry(value: Double(report.low_450),
                                             label: "Плохие - " + String(format:"%.1f", 100*report.low_450/amount) + " %"))
            entries.append(PieChartDataEntry(value: Double(report.from_450_to_650),
                                             label: "Средние - " + String(format:"%.1f", 100*report.from_450_to_650/amount) + " %"))
            entries.append(PieChartDataEntry(value: Double(report.over_650),
                                             label: "Хорошие - " + String(format:"%.1f", 100*report.over_650/amount) + " %"))
            
            let set = PieChartDataSet(values: entries, label: formatDate(date: report.periodDate!, year : true))
            set.drawIconsEnabled = false
            set.sliceSpace = 3
            
            
            set.colors = [MOCColor.Red.toUIColor(), MOCColor.Blue.toUIColor(), MOCColor.Green.toUIColor()]
            
            let data = PieChartData(dataSet: set)
            
            dataCollection.append(data)
        }
        
        return dataCollection.reversed()
    }
    
    
    func emptyPieData(date : Date) -> ChartData {
        var entries : [PieChartDataEntry] = []
        entries.append(PieChartDataEntry(value: 0.1,
                                         label: "Плохие - 0%"))
        entries.append(PieChartDataEntry(value: 0.1,
                                         label: "Средние - 0%"))
        entries.append(PieChartDataEntry(value: 0.1,
                                         label: "Хорошие - 0%"))
        let set = PieChartDataSet(values: entries, label: formatDate(date:date, year : true))
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        
        
        set.colors = [UIColor(hexString: "#E0E7E8"), UIColor(hexString: "#E0E7E8"), UIColor(hexString: "#E0E7E8")]
        return PieChartData(dataSet: set)
    }
    
    func indicatorsBarChartData() -> [ChartData]? {
        
        guard !reports.isEmpty else{
            return [emptyBarData(date: Date())]
        }
        
        var dataCollection :[ChartData] = []
        
        dateFormatterPrint.dateFormat = "MMMM"
        
        for (index, report) in reports.enumerated() where index < 6 {
            
            if report.fpd>=0{
                var entries : [BarChartDataEntry] = []
                
                entries.append(BarChartDataEntry(x: 0,
                                                 y: Double(report.fpd) * 100,
                                                 data: formatDate(date: report.periodDate!, year : false) as AnyObject))
                
                if reports.indices.contains(index+1){
                    let reportSpd = reports[index+1]
                   entries.append(BarChartDataEntry(x: 1,
                                                    y: Double(reportSpd.spd) * 100,
                                                    data:formatDate(date: reportSpd.periodDate!, year : false) as AnyObject))
                }
                
                if reports.indices.contains(index+2){
                    let reportTpd = reports[index+2]
                    entries.append(BarChartDataEntry(x: 2,
                                                     y: Double(reportTpd.tpd) * 100,
                                                     data:formatDate(date: reportTpd.periodDate!, year : false) as AnyObject))
                }
                
                let set1 = BarChartDataSet(values: entries, label: formatDate(date: report.periodDate!, year : true))
                set1.colors = [MOCColor.Blue.toUIColor(), MOCColor.Red.toUIColor(),MOCColor.Green.toUIColor()]
                set1.drawValuesEnabled = true
                set1.valueFormatter = BarIndicatorsAboveTitleFormatter()
                set1.valueFont = UIFont(name: "HelveticaNeue-Light", size: 13)!
                
                let data = BarChartData(dataSet: set1)
                
                dataCollection.append(data)
            }else{
                dataCollection.append(emptyBarData(date: report.periodDate!))
            }
        }
        
        return dataCollection.reversed()
    }
    
    func emptyBarData(date : Date) -> BarLineScatterCandleBubbleChartData {
        var entries : [BarChartDataEntry] = []
        
        var components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        dateFormatterPrint.dateFormat = "LLLL"
        
        entries.append(BarChartDataEntry(x: 0, y: 0.01, data: dateFormatterPrint.string(from: date) as AnyObject))
        
        components.day = 15 //если у пред месяца 31 число, то след месяц будет таким же
        components.month = components.month! - 1
        let spdDate = Calendar.current.date(from: components)
        entries.append(BarChartDataEntry(x: 1, y: 0.01, data: dateFormatterPrint.string(from: spdDate!) as AnyObject))
        
        components.day = 15
        components.month = components.month! - 1
        let tpdDate = Calendar.current.date(from: components)
        entries.append(BarChartDataEntry(x: 2, y: 0.01, data: dateFormatterPrint.string(from: tpdDate!) as AnyObject))
        
        let set1 = BarChartDataSet(values: entries, label: formatDate(date: date, year : true))
        set1.colors = [UIColor(hexString: "#E0E7E8"), UIColor(hexString: "#E0E7E8"),UIColor(hexString: "#E0E7E8")]
        set1.drawValuesEnabled = true
        set1.barBorderWidth = 4
        set1.barBorderColor = UIColor(hexString: "#E0E7E8")
        set1.valueFormatter = BarIndicatorsAboveTitleFormatter()
        set1.valueFont = UIFont(name: "HelveticaNeue-Light", size: 13)!
        
        return BarChartData(dataSet: set1)
    }
    
    func rejectValue() -> [RejectSignModel]? {
        
        guard !reports.isEmpty else{
            
            return [RejectSignModel(value: "0", diff: 0, date: formatDate(date: Date(), year : true))] }
        
        var dataCollection :[RejectSignModel] = []
        
        for (index, report) in reports.enumerated() where index < 6 {

            
            if report.reject < 0{
                dataCollection.append(RejectSignModel(value: "0", diff: 0.0, date: formatDate(date: report.periodDate!, year : true)))
                continue
            }
            
            
            var lastReject: Float = 0
            
            lastReject = report.reject
            
            var diff : Float = 0.0
            
            if  (reports.indices.contains(index + 1)) {
                let prev = Float((reports[index + 1].reject))
                if prev >= 0{
                    diff = Float(lastReject - prev)
                }
            }
            
            let value = String(describing: lastReject.rounded(toPlaces: 2)*100)
            
            dataCollection.append(RejectSignModel(value: value, diff: diff, date: formatDate(date: report.periodDate!, year : true)))
        }
        
        return dataCollection.reversed()
        
    }
    
    func formatDate(date : Date, year : Bool) -> String {
//        let month = Calendar.current.component(.month, from: date)
        dateFormatterPrint.dateFormat = "LLLL"
//        let months = ["январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"]
        let monthStr = dateFormatterPrint.string(from: date)
        if year {
            dateFormatterPrint.dateFormat = "yyyy"
            
            return monthStr + ", " + dateFormatterPrint.string(from: date)
        }
        
        return monthStr
    }
}
