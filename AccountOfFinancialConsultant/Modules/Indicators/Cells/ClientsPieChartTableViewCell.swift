//
//  ClientsPieChartTableViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

protocol ClientsPieChartProtocol : class {
     func clientsPieChartData() -> [ChartData]?
    func showClientChartDetail()
}

class ClientsPieChartTableViewCell: BaseChartTableViewCell, PagerCollectionViewDelegate {
    
    
    
    
//    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var pagerView: PagerCollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var datasource : [ChartData]?{
        didSet{
            guard self.datasource?.count != 0 else { return }
            pagerView.performBatchUpdates(nil) { (finish) in
                self.pagerView.showItem = (self.datasource?.count)! - 1
                self.dateLabel.text = (self.datasource!.last?.dataSets.first as! PieChartDataSet).label

            }
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 7
        containerView.clipsToBounds = true
        pagerView.pagerDelegate = self
        
    }
    
    func pager(_ pager: PagerCollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pager.dequeueReusableCell(withReuseIdentifier: "ClientCollectionViewCell", for: indexPath) as! ClientCollectionViewCell
        cell.configureWithData(data: datasource![indexPath.row])
        
        return cell
    }

    func pageWillDisplay(indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
        dateLabel.text = (datasource![indexPath.row].dataSets.first as! PieChartDataSet).label
    }
    
    func pager(_ pager: PagerCollectionView, didSeselectItemAt indexPath: IndexPath) {
        delegate?.showClientChartDetail()
    }
    

    override func configure() {
        if let data = self.delegate?.clientsPieChartData() {
            
            datasource = data
            pagerView.count = (datasource?.count)!
            pageControl.numberOfPages = (datasource?.count)!
            pagerView.reloadData()
        }
    }
    


}
