//
//  BaseChartTableViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

protocol BaseCellProtocol : ClientsPieChartProtocol, RejectProtocol, IndicatorsBarChartProtocol{
    func reportDate() -> String?
    
}

class BaseChartTableViewCell: UITableViewCell, ChartViewDelegate {

    weak var delegate: BaseCellProtocol?{
        didSet{
            configure()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configure(){
        
    }
}
