//
//  IndicatorsBarCollectionViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 18.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

class IndicatorsBarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var chartView: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupChart()
    }
    
    func setupChart() {
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.highlightFullBarEnabled = false
        chartView.highlightPerTapEnabled = false
        chartView.rightAxis.enabled = false
        chartView.drawGridBackgroundEnabled = false
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        
        let leftAxis = chartView.leftAxis
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.valueFormatter = PersentAbsAxisValueFormatter()
        leftAxis.labelCount = 5
        leftAxis.axisMinimum = 0
        
        let xAxis = chartView.xAxis
        xAxis.enabled = true
        xAxis.labelCount = 3
        xAxis.granularity = 1
        xAxis.drawLabelsEnabled = true
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        
        
        let l = chartView.legend
        l.enabled = false
        
    }
    
    func configureWithData(data: ChartData) {
        
        let barData : BarChartData = data as! BarChartData
        barData.barWidth = 0.6
        chartView.data = barData
        let dataset = barData.dataSets.first as! BarChartDataSet
        
        var titles : [String] = []
        
        for val in dataset.values where val.data != nil {
            let title = val.data as! String
            titles.append(title)
        }
        
        let xAxis = chartView.xAxis
        if titles.isEmpty {
            xAxis.drawLabelsEnabled = false
        }else{
            xAxis.valueFormatter = CustomTitlesAxisValueFormatter(titles: titles)
            xAxis.drawLabelsEnabled = true
            xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
            xAxis.labelTextColor = UIColor(hexString: "#98A4A9")
        }
        

        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 13)!
        leftAxis.labelTextColor = UIColor(hexString:"98A4A9")
        leftAxis.labelCount = 3
        leftAxis.axisMaximum = barData.yMax < 5 ? 5 : barData.yMax * 1.2
        leftAxis.labelPosition = .outsideChart
        
    }
}
