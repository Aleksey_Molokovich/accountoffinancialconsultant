//
//  RejectCollectionViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 18.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class RejectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var rejectValueLabel: UILabel!
    
    @IBOutlet weak var diffLabel: UILabel!
    @IBOutlet weak var markerUpImage: UIImageView!
    @IBOutlet weak var markerDownImage: UIImageView!
    
    func configureWithData(reject : RejectSignModel ) {
        rejectValueLabel.text = reject.value
        diffLabel.isHidden = false
        if (reject.diff > 0){
            markerUpImage.isHidden = false
            markerDownImage.isHidden = true
            diffLabel.textColor = MOCColor.Red.toUIColor()
            diffLabel.text = "+" + String(describing: (reject.diff * 100.0).rounded(toPlaces: 2)) + " %"
        }else if reject.diff < 0{
            markerUpImage.isHidden = true
            markerDownImage.isHidden = false
            diffLabel.textColor = MOCColor.Green.toUIColor()
            diffLabel.text = String(describing: (reject.diff * 100.0).rounded(toPlaces: 2)) + " %"
        }else{
            markerUpImage.isHidden = true
            markerDownImage.isHidden = true
            diffLabel.isHidden = true
        }
        
    }
}
