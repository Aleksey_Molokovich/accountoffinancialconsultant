//
//  ClientCollectionViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 17.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

class ClientCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var chartView: PieChartView!
    
    
    @IBOutlet weak var badLabel: UILabel!
    @IBOutlet weak var middleLabel: UILabel!
    @IBOutlet weak var goodLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        chartView.drawEntryLabelsEnabled = false
        chartView.usePercentValuesEnabled = false
        
        chartView.drawSlicesUnderHoleEnabled = false
        
        chartView.holeRadiusPercent = 0.6
        chartView.transparentCircleRadiusPercent = 0.6
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 10, right: 0, bottom: 5)
        
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.highlightPerTapEnabled = false
        
        let l = chartView.legend
        l.enabled = false
//        l.extraEntries = nil
        l.horizontalAlignment = .right
        l.verticalAlignment = .center
        l.orientation = .vertical
        l.font = UIFont(name: "HelveticaNeue-Light", size: 13)!
        l.xEntrySpace = 7
        l.yEntrySpace = 10
        l.yOffset = 0
        l.form = .circle
        
        chartView.data = nil
    }
    
    func configureWithData(data : ChartData){
        
        
        
        data.setDrawValues(false)
        chartView.data = data
        let pieData = data.dataSets.first as! PieChartDataSet
        
        for (index, value) in pieData.values.enumerated() {
            
            let pieValue = value as! PieChartDataEntry
            
            if index == 0{
                badLabel.text = pieValue.label
            }else if index == 1{
                middleLabel.text = pieValue.label
            }else{
                goodLabel.text = pieValue.label
            }
        }
        
        chartView.highlightValues(nil)

    }
}
