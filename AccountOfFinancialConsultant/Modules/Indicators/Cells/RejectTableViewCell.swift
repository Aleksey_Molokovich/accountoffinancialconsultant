//
//  RejectTableViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

protocol RejectProtocol : class {
    func rejectValue() -> [RejectSignModel]?
    func showRejectChartDetail()
}

class RejectTableViewCell: BaseChartTableViewCell, PagerCollectionViewDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pagerView: PagerCollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var datasource : [RejectSignModel]?{
        didSet{
            guard self.datasource?.count != 0 else { return }
            pagerView.performBatchUpdates(nil) { (finish) in
                self.pagerView.showItem = (self.datasource?.count)! - 1
                self.dateLabel.text = self.datasource!.last?.date
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 7
        containerView.clipsToBounds = true
        pagerView.pagerDelegate = self
    }
    
    override func configure() {
        if let data = self.delegate?.rejectValue(){
            datasource = data
            pagerView.count = (datasource?.count)!
            pageControl.numberOfPages = (datasource?.count)!
            pagerView.reloadData()
        }
    }
    
    func pager(_ pager: PagerCollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pager.dequeueReusableCell(withReuseIdentifier: "RejectCollectionViewCell", for: indexPath) as! RejectCollectionViewCell
        cell.configureWithData(reject: datasource![indexPath.row])
        
        return cell
    }
    
    func pageWillDisplay(indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
        self.dateLabel.text = self.datasource![indexPath.row].date
    }
    
    func pager(_ pager: PagerCollectionView, didSeselectItemAt indexPath: IndexPath) {
        delegate?.showRejectChartDetail()
    }
    
    
}
