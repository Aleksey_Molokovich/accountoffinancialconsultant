//
//  IndicatorsProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
import Charts
//MARK: Configurator -
protocol IndicatorsConfiguratorProtocol: class {
    func configure(with viewController: IndicatorsViewProtocol)
}

//MARK: Router -
protocol IndicatorsRouterProtocol: class {
    func pushChartDetailController(type : ChartType)
}
//MARK: Presenter -
protocol IndicatorsPresenterProtocol: class {

    var interactor: IndicatorsInteractorInputProtocol? { get set }
    var router: IndicatorsRouterProtocol! { set get }
    func viewWillAppear()
    func configureView()
    func cell(cellIindex : Int) -> String
}

//MARK: Interactor -
protocol IndicatorsInteractorOutputProtocol: class {
    func update(reports : [ReportModel]?)
    func fail(error : String)
    /* Interactor -> Presenter */
}

protocol IndicatorsInteractorInputProtocol: class {

    var presenter: IndicatorsInteractorOutputProtocol?  { get set }
    func readStore()
    func load()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol IndicatorsViewProtocol: class {

    var presenter: IndicatorsPresenterProtocol!  { get set }
    func navigation(title : String)
    func update()
    /* Presenter -> ViewController */
}
