//
//  RejectSignModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 18.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

class RejectSignModel {
    var value : String
    var diff : Float
    var date : String
    
    required init(value : String, diff : Float, date : String) {
        self.value = value
        self.diff = diff
        self.date = date
    }
}
