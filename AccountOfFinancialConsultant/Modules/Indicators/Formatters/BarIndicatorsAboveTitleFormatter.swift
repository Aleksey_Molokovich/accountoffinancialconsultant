//
//  BarIndicatorsAboveTitleFormatter.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import Charts

class BarIndicatorsAboveTitleFormatter: IValueFormatter {
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        switch entry.x {
        case 0:
            return "FPD " + String(format:"%.1f", value) + " %"
        case 1:
            return "SPD " + String(format:"%.1f", value) + " %"
        case 2:
            return "TPD " + String(format:"%.1f", value) + " %"
        default:
            return ""
        }
    }
}
