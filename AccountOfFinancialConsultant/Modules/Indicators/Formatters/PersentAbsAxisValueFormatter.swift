//
//  PersentAbsAxisValueFormatter.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import Charts

class PersentAbsAxisValueFormatter: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return String(abs(Int(value))) + " %"
    }
    
    
}
