//
//  CustomTitlesAxisValueFormatter.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 09.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import Charts

class CustomTitlesAxisValueFormatter: IAxisValueFormatter {
    
    let titles : [String]
    
    required init(titles:[String]) {
        self.titles = titles
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        let index = axis?.entries.index(of: value)
        if Int(value)<titles.count && value >= 0{
            return self.titles[Int(value)]
        }
        return ""
    }
}
