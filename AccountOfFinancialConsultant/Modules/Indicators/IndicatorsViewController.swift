//
//  IndicatorsViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class IndicatorsViewController: UIViewController, IndicatorsViewProtocol, UITableViewDelegate, UITableViewDataSource {
    var presenter: IndicatorsPresenterProtocol!
    

    let configurator: IndicatorsConfiguratorProtocol = IndicatorsConfigurator()
    
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Lifecycle methods
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configurator.configure(with: self)
    }
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.configureView()
        
//        UIFont.familyNames.forEach({ familyName in
//            let fontNames = UIFont.fontNames(forFamilyName: familyName)
//            print(familyName, fontNames)
//        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    // MARK: - Action methods
    
    
    // MARK: - TableViewDelegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cellReuseIdentifier = presenter.cell(cellIindex: indexPath.row)
        
        let cell:BaseChartTableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! BaseChartTableViewCell?)!
        
        cell.delegate = (presenter as! BaseCellProtocol)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//        presenter.selectChart(index: indexPath.row)
//    }

    // MARK: - ViewProtocol methods
    
    func navigation(title: String) {
        self.navigationItem.title = title
    }
    
    func update() {
        let offset = tableView.contentOffset
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
        self.tableView.contentOffset = offset
        
    }
    
}
