//
//  IndicatorsInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import ObjectMapper

class IndicatorsInteractor: IndicatorsInteractorInputProtocol {

    var operation:Operation?
    
    var lastUpdate : Date?
    
    weak var presenter: IndicatorsInteractorOutputProtocol?
    
    required init(presenter: IndicatorsInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    
    func load() {
        
        #if DEBUGMOCK
        let dao = Services.shared.dao
        dao.deleteAllTasks(with:({_,_ in }))
        dao.deleteAllReports(with: ({_,_ in }))
        readFromFile()
        return
        #endif
        

        
        if operation != nil && (operation?.isExecuting)!  { return }
        
        if lastUpdate != nil && Date().timeIntervalSince(lastUpdate!) < 60 {
            return
        }
        
        operation = API.reports(filter:.month, completion: {[weak self] (json) in
            guard let sSelf = self else { return }
            let items = Mapper<ReportModel>().mapArray(JSONObject: json)!
            sSelf.updateStore(items: items)
            sSelf.lastUpdate = Date()
//            print(sSelf.lastUpdate as Any)
        }) {[weak self] (error) in
            guard let sSelf = self else { return }
            sSelf.presenter?.fail(error: "")
        }
        
        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
    }
    
    func updateStore(items : [ReportModel]) {
        let dao = Services.shared.dao
        
        var i : Int = 0
        for item in items {
            dao.addOrUpdateReport(item: item) { [weak self] (Succes, Err) in
                guard let strongSelf = self else { return }
                
                i += 1
                if i == items.count {
                    strongSelf.presenter?.update(reports: items)
                }
            }
        }
    }
    
    
    func readStore()  {
        
        let dao = Services.shared.dao
//        dao.deleteAllReports { (succes, error) in
//            
//        }
        
        dao.reports {[weak self] (items, Err) in
            guard let strongSelf = self else { return }
            
            guard let _ = items else{ return }
            
            strongSelf.presenter?.update(reports:items!)
        }
    }
    
    func readFromFile() {
        
        let path = Bundle.main.path(forResource: "Reports", ofType: "json")
        
        do {
            let fileUrl = URL(fileURLWithPath: path!)
            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try! JSONSerialization.jsonObject(with: jsonData)
            let items = Mapper<ReportModel>().mapArray(JSONObject: json)!
            self.updateStore(items: items)
        } catch {
            
        }
    }
}
