//
//  IndicatorsRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit


class IndicatorsRouter: IndicatorsRouterProtocol {

    weak var viewController: UIViewController?

    init(viewController: IndicatorsViewProtocol) {
        self.viewController = viewController as? UIViewController
    }
    
    func pushChartDetailController(type : ChartType) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        var nextViewController : UIViewController?
        
        if type == .indicators {
            nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailIndicatorsViewController")
        }else{
            nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailIndicatorsClientsViewController")

            (nextViewController as! DetailIndicatorsClientsViewProtocol).presenter.configureChart(type: type)
            
        }
        viewController?.navigationController?.pushViewController(nextViewController! , animated: true)

    }
    
}
