//
//  AuthorizationPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
//import LGAlertView

class AuthorizationPresenter: AuthorizationPresenterProtocol, AuthorizationInteractorOutputProtocol {
    
    

    weak private var view: AuthorizationViewProtocol?
    var interactor: AuthorizationInteractorInputProtocol?
    var router: AuthorizationRouterProtocol!

    init(view: AuthorizationViewProtocol) {
        self.view = view
    }


    func configureView() {
        
    }
    
    
    func verifyPhoneRequest(phone : String) {
        view?.showActivity()
        interactor?.smsRequest(phone : phone)
    }
    
    func showAbout() {
        router.pushAboutController()
    }
    
    //MARK: InteractorOutputProtocol -
    
    func succesful() {
        view?.hideActivity()
        router.pushVerifySMSCodeController()
    }
    
    func fail(warrning : String?, error : String?) {
        view?.hideActivity()
        if warrning != nil {
            view?.changeStatePhoneView(wrong : true)
            view?.changeInfo(text: warrning!)
        }
        
        if error != nil {
            
            router.presentAlert(text : error!)
        }
    }
    
   
    
    
}
