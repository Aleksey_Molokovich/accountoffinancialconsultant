//
//  AuthorizationProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol AuthorizationConfiguratorProtocol: class {
    func configure(with viewController: AuthorizationViewProtocol)
}

//MARK: Router -
protocol AuthorizationRouterProtocol: class {
    func closeCurrentViewController()
    func pushVerifySMSCodeController()
    func pushAboutController()
    func presentAlert(text : String)
}
//MARK: Presenter -
protocol AuthorizationPresenterProtocol: class {

    var interactor: AuthorizationInteractorInputProtocol? { get set }
    var router: AuthorizationRouterProtocol! { set get }
    func configureView()
    func verifyPhoneRequest(phone : String)
    func showAbout()
}

//MARK: Interactor -
protocol AuthorizationInteractorOutputProtocol: class {
    
    func succesful()
    func fail(warrning : String?, error : String?)
    /* Interactor -> Presenter */
}

protocol AuthorizationInteractorInputProtocol: class {

    var presenter: AuthorizationInteractorOutputProtocol?  { get set }
    func smsRequest(phone : String)
    /* Presenter -> Interactor */
}

//MARK: View -
protocol AuthorizationViewProtocol: class {

    var presenter: AuthorizationPresenterProtocol!  { get set }
    func changeStatePhoneView(wrong : Bool)
    func changeInfo(text : String)
    func showActivity()
    func hideActivity()
    /* Presenter -> ViewController */
}
