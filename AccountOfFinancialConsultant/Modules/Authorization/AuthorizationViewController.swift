//
//  AuthorizationViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit


class AuthorizationViewController: UIViewController, AuthorizationViewProtocol {

	var presenter: AuthorizationPresenterProtocol!

    let configurator: AuthorizationConfiguratorProtocol = AuthorizationConfigurator()

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var phoneTextField: AFCPhoneTextField!
    @IBOutlet weak var textContainerView: RoundedView!

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var verifyButton: BigBlueButton!
    
    var observation: NSKeyValueObservation!
    
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
        activityIndicator.isHidden = true
        self.verifyButton.isEnabled = false
        textContainerView.layer.borderWidth = 1
        textContainerView.layer.borderColor = UIColor.init(hexString: "#E0E7E8").cgColor
        
        observation = phoneTextField.observe(\.deformattedText){ (phoneTextField, change) in
            self.infoLabel.text = "На указанный номер телефона будет\n отправлено СМС-сообщение с кодом"
            self.infoLabel.textColor = UIColor(hexString: "#98A4A9")
            if phoneTextField.deformattedText != nil
            {
                self.verifyButton.isEnabled = true
            }else{
                self.verifyButton.isEnabled = false
            }
            self.changeStatePhoneView(wrong : false)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        #if DEBUG
//        phoneTextField.text = "9050392048"
        #endif

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            //            let endFrameY = endFrame?.origin.y ??

            self.bottomConstraint.constant = (endFrame?.size.height)! - 30
            UIView.animate(withDuration: 0,
                           delay:0,
                           options: .curveEaseInOut,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        self.bottomConstraint.constant = 70
        UIView.animate(withDuration: 0,
                       delay:0,
                       options: .curveEaseInOut,
                       animations: { self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }

    deinit {
        observation = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    // MARK: - Action methods
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        presenter.verifyPhoneRequest(phone: phoneTextField.deformattedText!)
    }
    
    
    @IBAction func clickAbout(_ sender: Any) {
        presenter.showAbout()
    }
    

    // MARK: - ViewProtocol methods
    
    func changeStatePhoneView(wrong : Bool){
        if wrong {
           textContainerView.layer.borderColor = UIColor.red.cgColor
            phoneTextField.textColor = UIColor.red
        }else{
            textContainerView.layer.borderColor = UIColor.init(hexString: "#E0E7E8").cgColor
            phoneTextField.textColor = UIColor.black
        }
    }
    
    func changeInfo(text: String) {
        infoLabel.text = text
        infoLabel.textColor = MOCColor.Red.toUIColor()
    }
    
    func showActivity(){
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivity(){
        activityIndicator.stopAnimating()
    }
}
