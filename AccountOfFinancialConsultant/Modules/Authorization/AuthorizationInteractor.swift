//
//  AuthorizationInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class AuthorizationInteractor: AuthorizationInteractorInputProtocol {

    weak var presenter: AuthorizationInteractorOutputProtocol?
    
    var operation:Operation?
    
    required init(presenter: AuthorizationInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func smsRequest(phone : String) {
        
        operation = API.sendSMSTo(phone: phone, completion: {[weak self] (json) in
            guard let sSelf = self else { return }
            DispatchQueue.main.async {
                sSelf.presenter?.succesful()
            }
            
        }) { (error) in
            
            
            DispatchQueue.main.async {[unowned self] in
                let code = error.statusCode()
                
                if let codeDescription = error.statusCodeDescription() {
                    self.presenter?.fail(warrning :nil, error : codeDescription)
                } else {
                    if code == 404{
                        self.presenter?.fail(warrning : "Пользователь с указанным номером\nне найден.", error : nil)
                    }else{
                       self.presenter?.fail(warrning : nil, error : nil)
                    }
                }                
            }
        }

        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
    }
}
