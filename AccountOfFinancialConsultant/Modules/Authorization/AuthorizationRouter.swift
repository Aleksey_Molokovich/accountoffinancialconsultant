//
//  AuthorizationRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class AuthorizationRouter: AuthorizationRouterProtocol {
    
    
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

    weak var viewController: UIViewController?

    init(viewController: AuthorizationViewProtocol) {
        self.viewController = viewController as? UIViewController
    }
    

    
    func closeCurrentViewController() {
    }
    
    func pushVerifySMSCodeController() {
        
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VerificationSMSViewController") as UIViewController
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func pushAboutController() {
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AboutViewController") as UIViewController
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func presentAlert(text : String){
        let alert = UIAlertController(title: "Ошибка", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: nil))
        self.viewController?.present(alert, animated: true, completion: nil)
    }
}
