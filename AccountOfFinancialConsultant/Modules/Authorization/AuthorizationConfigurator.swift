//
//  AuthorizationConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 14.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class AuthorizationConfigurator: AuthorizationConfiguratorProtocol {

    func configure(with viewController: AuthorizationViewProtocol) {
        let presenter = AuthorizationPresenter(view: viewController)
        let interactor = AuthorizationInteractor(presenter: presenter)
        let router = AuthorizationRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
