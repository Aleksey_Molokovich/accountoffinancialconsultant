//
//  NotificationSetupProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol NotificationSetupConfiguratorProtocol: class {
    func configure(with viewController: NotificationSetupViewProtocol)
}

//MARK: Presenter -
protocol NotificationSetupPresenterProtocol: class {

    var interactor: NotificationSetupInteractorInputProtocol? { get set }
    func configureView()
    func cellData(atIndex : Int) -> (text: String, flag: Bool)
    func changeConfig(atIndex: Int)
}

//MARK: Interactor -
protocol NotificationSetupInteractorOutputProtocol: class {
    func succesful()
    func fail(error : String)
    /* Interactor -> Presenter */
}

protocol NotificationSetupInteractorInputProtocol: class {

    var presenter: NotificationSetupInteractorOutputProtocol?  { get set }
    
    /* Presenter -> Interactor */
}

//MARK: View -
protocol NotificationSetupViewProtocol: class {

    var presenter: NotificationSetupPresenterProtocol!  { get set }
    func update()
    /* Presenter -> ViewController */
}
