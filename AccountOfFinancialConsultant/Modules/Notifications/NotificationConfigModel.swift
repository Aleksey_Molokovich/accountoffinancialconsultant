//
//  NotificationSetupModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

class NotificationConfigModel : Codable  {
    
    var messageFromChat : Bool = true
    var newOpenTask : Bool = true
    var changePriorityTask : Bool = true
    var trasitionToOpen : Bool = true
    var changeDeadline : Bool = true
    
}
