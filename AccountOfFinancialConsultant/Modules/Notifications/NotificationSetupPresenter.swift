//
//  NotificationSetupPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class NotificationSetupPresenter: NotificationSetupPresenterProtocol, NotificationSetupInteractorOutputProtocol {

    weak private var view: NotificationSetupViewProtocol?
    var interactor: NotificationSetupInteractorInputProtocol?

    init(view: NotificationSetupViewProtocol) {
        self.view = view
    }

    func configureView() {
        
    }
    
    func cellData(atIndex : Int) -> (text: String, flag: Bool){
        let data = Services.shared.accountService.notificationConfig
        switch atIndex {
        case 0:
            return ("Сообщения из чата", data.messageFromChat)
        case 1:
            return ("Новая открытая задача", data.newOpenTask)
        case 2:
            return ("Изменение приоритета задачи", data.changePriorityTask)
        case 3:
            return ("Переход из закрытой в открытую", data.trasitionToOpen)
        case 4:
            return ("Изменение срока задачи", data.changeDeadline)

        default:
            return ("", false)
        }
    }
    
    func changeConfig(atIndex: Int)  {
        let data = Services.shared.accountService.notificationConfig
        switch atIndex {
        case 0:
             data.messageFromChat = !data.messageFromChat
        case 1:
            data.newOpenTask = !data.newOpenTask
        case 2:
            data.changePriorityTask = !data.changePriorityTask
        case 3:
            data.trasitionToOpen = !data.trasitionToOpen
        case 4:
            data.changeDeadline = !data.changeDeadline
            
        default:
            break
        }
        
        Services.shared.accountService.notificationConfig = data
        view?.update()
    }
    
    //MARK: InteractorOutputProtocol -
    
    func succesful() {
    }
    
    func fail(error: String) {
    }
}
