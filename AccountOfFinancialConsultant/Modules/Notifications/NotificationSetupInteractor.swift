//
//  NotificationSetupInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class NotificationSetupInteractor: NotificationSetupInteractorInputProtocol {

    weak var presenter: NotificationSetupInteractorOutputProtocol?
    
    required init(presenter: NotificationSetupInteractorOutputProtocol) {
        self.presenter = presenter
    }
}
