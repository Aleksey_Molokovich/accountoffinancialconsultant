//
//  NotificationSetupConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class NotificationSetupConfigurator: NotificationSetupConfiguratorProtocol {

    func configure(with viewController: NotificationSetupViewProtocol) {
        let presenter = NotificationSetupPresenter(view: viewController)
        let interactor = NotificationSetupInteractor(presenter: presenter)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
