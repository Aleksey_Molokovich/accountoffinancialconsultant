//
//  NotificationSetupViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class NotificationSetupViewController: UIViewController, NotificationSetupViewProtocol, UITableViewDelegate, UITableViewDataSource {

	var presenter: NotificationSetupPresenterProtocol!

    let configurator: NotificationSetupConfiguratorProtocol = NotificationSetupConfigurator()

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    // MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationSetupTableViewCell", for: indexPath) as! NotificationSetupTableViewCell
        
        cell.label.text = presenter.cellData(atIndex: indexPath.row).text
        cell.checkImage.isHidden = !presenter.cellData(atIndex: indexPath.row).flag
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        presenter.changeConfig(atIndex: indexPath.row)
    }
    
    // MARK: - ViewProtocol methods
    
    func update() {
        tableView.reloadData()
    }
}
