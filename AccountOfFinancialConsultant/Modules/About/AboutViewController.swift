//
//  AboutViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class AboutViewController: UIViewController {



    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = MOCColor.Blue.toUIColor()

    }
    
    
}
