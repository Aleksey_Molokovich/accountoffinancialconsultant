//
//  ChatListRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatListRouter: ChatListRouterProtocol {

    weak var viewController: UIViewController?

    init(viewController: ChatListViewProtocol) {
        self.viewController = viewController as? UIViewController
    }
    

    
    func pushChatController() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        let output = (viewController as! ChatListViewController).presenter
        nextViewController.configure(outputMudule: output as! ChatOutputModuleProtocol)
        
        viewController?.navigationController?.pushViewController(nextViewController as UIViewController, animated: true)
        
    }
}
