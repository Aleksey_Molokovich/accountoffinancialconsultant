//
//  ChatListPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

struct ChatList {
    var url : String!
    var id : Int!
    var name : String!
    var lastMessage : String?
    var timeMessage : String?
}

class ChatListPresenter: ChatListPresenterProtocol, ChatListInteractorOutputProtocol, ChatOutputModuleProtocol {

    weak private var view: ChatListViewProtocol?
    var interactor: ChatListInteractorInputProtocol?
    var router: ChatListRouterProtocol!
    var selectedChatId : Int?
    var chats : [ChatList] = []

    let dateFormatter = DateFormatter()
    
    init(view: ChatListViewProtocol) {
        self.view = view
        chats =  [ChatList(url: "wss://crcal.sovcombank.ru/chat/ws", id: 0, name: "Техподдержка", lastMessage: "", timeMessage: "")]
        
    }

    func chatConfig() -> (chatId: Int, url: String) {
        let chat = chats.filter({ $0.id == 0 }).first
        return (chatId: chat!.id, url: (chat?.url)!)
    }
    
    func configureView() {
        
    }
    
    func viewWillAppear(){
        interactor?.lastMessage(chatId: (chats.first?.id)!)
    }
    
    func numbersOfChat() -> Int {
        return chats.count
    }
    func cellData(atIndex: Int) -> (name: String, message: String, time: String) {
        return (name : chats[atIndex].name,
                message : chats[atIndex].lastMessage!,
                time: chats[atIndex].timeMessage!)
    }
    
    func openChat(atIndex: Int) {
        selectedChatId = atIndex
        router.pushChatController()
    }
    
    //MARK: InteractorOutputProtocol -
    func chatInfo(chatId: Int, message: String, time: Date) {
        
        if Date().timeIntervalSince(time) < 60*60*24{
           dateFormatter.dateFormat = "HH : mm"
        }else{
            dateFormatter.dateFormat = "d.MMM"
        }
        
        chats[chatId].lastMessage = message
        chats[chatId].timeMessage = dateFormatter.string(from: time)
        view?.update()
    }
    
    func succesful() {
        view?.hideActivity()
    }
    
    func fail(error: String) {
        view?.hideActivity()
    }
}
