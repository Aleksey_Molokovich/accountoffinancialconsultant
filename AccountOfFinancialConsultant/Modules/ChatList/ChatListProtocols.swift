//
//  ChatListProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol ChatListConfiguratorProtocol: class {
    func configure(with viewController: ChatListViewProtocol)
}

//MARK: Router -
protocol ChatListRouterProtocol: class {
    func pushChatController()
}
//MARK: Presenter -
protocol ChatListPresenterProtocol: class {

    var interactor: ChatListInteractorInputProtocol? { get set }
    var router: ChatListRouterProtocol! { set get }
    func configureView()
    func viewWillAppear()
    func numbersOfChat() -> Int
    func cellData(atIndex: Int) -> (name: String, message: String, time: String)
    func openChat(atIndex: Int)
}

//MARK: Interactor -
protocol ChatListInteractorOutputProtocol: class {
    func chatInfo(chatId: Int, message : String, time : Date)
    func fail(error : String)
    /* Interactor -> Presenter */
}

protocol ChatListInteractorInputProtocol: class {

    var presenter: ChatListInteractorOutputProtocol?  { get set }
    func lastMessage(chatId : Int)
    /* Presenter -> Interactor */
}

//MARK: View -
protocol ChatListViewProtocol: class {

    var presenter: ChatListPresenterProtocol!  { get set }
    func update()
    func showActivity()
    func hideActivity()
    /* Presenter -> ViewController */
}
