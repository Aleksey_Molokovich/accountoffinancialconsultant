//
//  ChatListInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatListInteractor: ChatListInteractorInputProtocol {

    weak var presenter: ChatListInteractorOutputProtocol?
    
    required init(presenter: ChatListInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    let dao = Services.shared.dao
    
    func lastMessage(chatId : Int)  {
        dao.messages { (list, error) in
            
            if error != nil || (list != nil && (list?.isEmpty)!) { return }
            
            let message = list?.sorted(by: { $0.date! - $1.date! > 0 }).first
            
            
            self.presenter?.chatInfo(chatId: chatId, message: (message?.text)!, time: Date(timeIntervalSince1970: (message?.date)!))
        }
    }
}
