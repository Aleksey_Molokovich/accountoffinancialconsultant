//
//  ChatListTableViewCell.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var lastMessageLabel: UILabel!
    
    @IBOutlet weak var circleView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        circleView.layer.cornerRadius = circleView.frame.height/2
        circleView.clipsToBounds = true
        circleView.layer.borderColor = UIColor(hexString: "#E0E7E8").cgColor
        circleView.layer.borderWidth = 2
        
    }


}
