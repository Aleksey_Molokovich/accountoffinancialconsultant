//
//  ChatListConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatListConfigurator: ChatListConfiguratorProtocol {

    func configure(with viewController: ChatListViewProtocol) {
        let presenter = ChatListPresenter(view: viewController)
        let interactor = ChatListInteractor(presenter: presenter)
        let router = ChatListRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
