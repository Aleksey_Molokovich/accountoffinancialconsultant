//
//  ChatListViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatListViewController: UIViewController, ChatListViewProtocol, UITableViewDataSource, UITableViewDelegate {

	var presenter: ChatListPresenterProtocol!

    let configurator: ChatListConfiguratorProtocol = ChatListConfigurator()

    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle methods
    
	override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    // MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numbersOfChat()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell", for: indexPath) as! ChatListTableViewCell
        cell.label.text = presenter.cellData(atIndex: indexPath.row).name
        cell.lastMessageLabel.text = presenter.cellData(atIndex: indexPath.row).message
        cell.timeLabel.text = presenter.cellData(atIndex: indexPath.row).time
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.openChat(atIndex: indexPath.row)
    }

    // MARK: - ViewProtocol methods
    
    func update() {
        tableView.reloadData()
    }
    
    func showActivity(){
    }
    
    func hideActivity(){
    }
}
