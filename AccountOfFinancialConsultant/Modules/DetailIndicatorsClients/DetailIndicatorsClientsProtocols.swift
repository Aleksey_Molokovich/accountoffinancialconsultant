//
//  DetailIndicatorsClientsProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 18.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
enum ChartType {
    case clients
    case indicators
    case reject
}

import Charts
//MARK: Configurator -
protocol DetailIndicatorsClientsConfiguratorProtocol: class {
    func configure(with viewController: DetailIndicatorsClientsViewProtocol)
}

//MARK: Router -
protocol DetailIndicatorsClientsRouterProtocol: class {
    func closeCurrentViewController()
}
//MARK: Presenter -
protocol DetailIndicatorsClientsPresenterProtocol: class {

    var interactor: DetailIndicatorsClientsInteractorInputProtocol? { get set }
    func configureChart(type : ChartType)
    func titles() -> [String]
    func switchPeriod(toWeek:Bool)
    func configureView()
}

//MARK: Interactor -
protocol DetailIndicatorsClientsInteractorOutputProtocol: class {
    
    func update(reports : [ReportModel]?)
    /* Interactor -> Presenter */
}

protocol DetailIndicatorsClientsInteractorInputProtocol: class {

    var presenter: DetailIndicatorsClientsInteractorOutputProtocol?  { get set }
    func readStore()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol DetailIndicatorsClientsViewProtocol: class {

    var presenter: DetailIndicatorsClientsPresenterProtocol!  { get set }
    func navigation(title : String)
    func updateChartDataClient(data : BarChartData?)
    func updateChartDataReject(data : BarChartData?)
    /* Presenter -> ViewController */
}
