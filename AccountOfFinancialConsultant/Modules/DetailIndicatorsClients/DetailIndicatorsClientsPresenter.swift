//
//  DetailIndicatorsClientsPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 18.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Charts

class DetailIndicatorsClientsPresenter: DetailIndicatorsClientsPresenterProtocol, DetailIndicatorsClientsInteractorOutputProtocol {

    weak private var view: DetailIndicatorsClientsViewProtocol?
    var interactor: DetailIndicatorsClientsInteractorInputProtocol?

    var xAxisTitles : [String] = []
    var isWeek = false
    var chartType :ChartType = .clients
    
    init(view: DetailIndicatorsClientsViewProtocol) {
        self.view = view
    }
    
    func configureChart(type: ChartType) {
        self.chartType = type
        
        view?.navigation(title: type == .clients ? "Клиенты" : "Процент отказов")
    }

    func configureView() {
        interactor?.readStore()
    }
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.negativeSuffix = " "
        formatter.positiveSuffix = " "
        
        return formatter
    }()
    
    func titles() -> [String] {
        return xAxisTitles
    }
    
    func switchPeriod(toWeek: Bool) {
        isWeek = toWeek
        interactor?.readStore()
    }
    
    func update(reports: [ReportModel]?) {
        guard let filtered = reports?.filter({$0.period == (self.isWeek ? "week" : "month")}), filtered.count > 0 else {
            view?.updateChartDataClient(data: nil)
            return
        }
        
        let sorted = filtered.sorted(by: { $0.periodDate?.compare($1.periodDate!) == .orderedDescending })
        
        var yValues : [BarChartDataEntry] = []
        self.xAxisTitles = []
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = isWeek ? "d MMM" : "MMM"
        
        var ranged = sorted
        let maxCount = 5
        if ranged.count > maxCount {
            ranged = Array(sorted[...maxCount])
        }
        
        ranged = ranged.sorted(by: { $1.periodDate?.compare($0.periodDate!) == .orderedDescending })
        
        for (index, report) in (ranged.enumerated()) {
            
            let monthIndex = Calendar.current.component(.month, from: report.periodDate!)
            let months = ["янв","фев","мар","апр","май","июн","июл","авг","сен","окт","ноя","дек"]
            let month = months[monthIndex - 1]
            if isWeek {
                dateFormatterPrint.dateFormat = "d"
                let day = dateFormatterPrint.string(from: report.periodDate!)
                self.xAxisTitles.append(day + " " + month )
            }else{
                self.xAxisTitles.append(month)
            }
            
            switch self.chartType {
            case .clients:
                if report.low_450 == 0 &&
                    report.from_450_to_650 == 0 &&
                    report.over_650 == 0 {
                    yValues.append(BarChartDataEntry(x: Double(index), yValues: []))
                    continue
                }
                yValues.append(BarChartDataEntry(x: Double(index), yValues: [Double(report.low_450), Double(report.from_450_to_650), Double(report.over_650)]))
            case .reject:
                if report.reject < 0 {
                    yValues.append(BarChartDataEntry(x: Double(index), yValues: []))
                }else{
                    yValues.append(BarChartDataEntry(x: Double(index), yValues: [Double(100-report.reject*100), -Double(report.reject*100)]))

                }
            case .indicators :break
            }
        }
        
        
        let set = BarChartDataSet(values: yValues, label: "")
        set.drawIconsEnabled = false
        
        switch self.chartType {
        case .clients:
            set.colors = [MOCColor.Red.toUIColor(), MOCColor.Blue.toUIColor(), MOCColor.Green.toUIColor()]
            set.stackLabels = ["Плохие", "Средние", "Хорошие"]
        case .reject:
            set.colors = [MOCColor.Green.toUIColor(), MOCColor.Red.toUIColor()]
            set.stackLabels = ["Одобрено", "Отказано"]
        case .indicators :break
        }
        
        
        set.barBorderColor = UIColor.white
        set.barBorderWidth = 1
        set.drawValuesEnabled = false
        let data = BarChartData(dataSet: set)
        data.barWidth = 0.55
        
        switch self.chartType {
        case .clients: view?.updateChartDataClient(data: data)
        case .reject: view?.updateChartDataReject(data: data)
        case .indicators :break
        }
        
    }
    
    
}
