//
//  DetailIndicatorsClientsInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 18.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class DetailIndicatorsClientsInteractor: DetailIndicatorsClientsInteractorInputProtocol {

    weak var presenter: DetailIndicatorsClientsInteractorOutputProtocol?
    
    required init(presenter: DetailIndicatorsClientsInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func readStore()  {
        
        let dao = Services.shared.dao
        
        dao.reports {[weak self] (items, Err) in
            guard let strongSelf = self else { return }
            
            guard let _ = items else{ return }
            
            strongSelf.presenter?.update(reports:items!)
        }
    }
}
