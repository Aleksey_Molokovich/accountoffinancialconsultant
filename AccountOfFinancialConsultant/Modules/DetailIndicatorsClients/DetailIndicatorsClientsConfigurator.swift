//
//  DetailIndicatorsClientsConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 18.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class DetailIndicatorsClientsConfigurator: DetailIndicatorsClientsConfiguratorProtocol {

    func configure(with viewController: DetailIndicatorsClientsViewProtocol) {
        let presenter = DetailIndicatorsClientsPresenter(view: viewController)
        let interactor = DetailIndicatorsClientsInteractor(presenter: presenter)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
