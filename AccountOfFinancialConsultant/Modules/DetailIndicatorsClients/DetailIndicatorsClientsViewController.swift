//
//  DetailIndicatorsClientsViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 18.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Charts





class DetailIndicatorsClientsViewController: UIViewController, DetailIndicatorsClientsViewProtocol, ChartViewDelegate {

	var presenter: DetailIndicatorsClientsPresenterProtocol!

    let configurator: DetailIndicatorsClientsConfiguratorProtocol = DetailIndicatorsClientsConfigurator()
    
    @IBOutlet weak var blankView: BlankView!
    @IBOutlet weak var legendClients: UIStackView!
    @IBOutlet weak var legendRejects: UIStackView!
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        formatter.negativeSuffix = " "
        formatter.positiveSuffix = " "
        
        return formatter
    }()
    

    
    @IBOutlet weak var barChartView: BarChartView!
    
    @IBOutlet weak var segmentedControl: AFCSegmentControl!
    
    // MARK: - Lifecycle methods
    
    convenience init() {
        self.init()
        configurator.configure(with: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configurator.configure(with: self)
    }
    
	override func viewDidLoad() {
        super.viewDidLoad()
        blankView.isHidden = true
        blankView.blankType = .noChartsData
        createSegmentedControl()
        commonConfigureCgartView()
        presenter.configureView()
    }
    

    func commonConfigureCgartView(){
        barChartView.fitBars = true
        barChartView.fitScreen()
        barChartView.delegate = self
        
        barChartView.chartDescription?.enabled = false
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = false
        barChartView.scaleXEnabled = false
        barChartView.scaleYEnabled = false
        
        barChartView.extraTopOffset = 20
        
        
        let leftAxis = barChartView.leftAxis
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: formatter)
        leftAxis.drawGridLinesEnabled = true
        leftAxis.drawZeroLineEnabled = false
        leftAxis.labelTextColor = UIColor(hexString: "#98A4A9")
        leftAxis.axisLineColor = MOCColor.GridLine.toUIColor()
        leftAxis.drawAxisLineEnabled = false
        leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
        leftAxis.gridColor = MOCColor.GridLine.toUIColor()
        
        barChartView.rightAxis.enabled = false
        
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
        xAxis.labelTextColor = UIColor(hexString: "#98A4A9")
        xAxis.axisLineColor = MOCColor.GridLine.toUIColor()
        
        let l = barChartView.legend
        l.enabled = false
        legendClients.isHidden = true
        legendRejects.isHidden = true

    }
    
    
    func createSegmentedControl() {
        segmentedControl.backgroundColor = .white
        segmentedControl.commaSeperatedButtonTitles = "Еженедельный,Ежемесячный"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(control:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 1
        
    }
    
   
    // MARK: - Action methods
    
    @objc func onChangeOfSegment(control :AFCSegmentControl ) {
        let period = control.selectedSegmentIndex == 0 ? true : false
        presenter.switchPeriod(toWeek : period)
        barChartView.highlightValue(x: 0, dataSetIndex: 100, stackIndex: 0)
    }

    // MARK: - ViewProtocol methods
    
    func navigation(title : String) {
        self.navigationItem.title = title
    }
    
    func updateChartDataClient(data : BarChartData?) {
        
        guard data != nil else {
            barChartView.clear()
            legendClients.isHidden = true
            legendRejects.isHidden = true
            blankView.isHidden = false
            return
        }
        blankView.isHidden = true
        legendClients.isHidden = false
        legendRejects.isHidden = true
        
        barChartView.marker = BarChartMarker(type: .clients)
        barChartView.highlightFullBarEnabled = true

        let leftAxis = barChartView.leftAxis
        leftAxis.axisMinimum = 0
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        
        let xAxis = barChartView.xAxis
        xAxis.labelCount = presenter.titles().count
        xAxis.valueFormatter = CustomTitlesAxisValueFormatter(titles: presenter.titles())
        
        barChartView.data = data
    }
    
    func updateChartDataReject(data : BarChartData?) {
        
        guard data != nil else {
            barChartView.clear()
            legendClients.isHidden = true
            legendRejects.isHidden = true
            blankView.isHidden = false
            return
        }
        blankView.isHidden = true
        legendClients.isHidden = true
        legendRejects.isHidden = false
        
        barChartView.marker = BarChartMarker(type: .reject)
        barChartView.highlightFullBarEnabled = false
        
        let leftAxis = barChartView.leftAxis
        leftAxis.axisMinimum = (data?.yMin)! - 20
        
        leftAxis.valueFormatter = PersentAbsAxisValueFormatter()
        
        let xAxis = barChartView.xAxis
        xAxis.labelCount = presenter.titles().count
        xAxis.valueFormatter = CustomTitlesAxisValueFormatter(titles: presenter.titles())
        
        barChartView.data = data
    }
    
}
