//
//  ChartMarkerViewProtocol.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 06.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

protocol ChartMarkerViewProtocol {
    func imageView(pading: CGFloat, pointDown : Bool? ) -> UIImage
}
