//
//  ChartPopupView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 05.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation


class BarChartPopupView: UIView, ChartMarkerViewProtocol {
    

    
    
    let cursorView = UIView()
    
    required init(values : [Double]) {
        super.init(frame: CGRect(x: 0, y: 0, width: 145, height: 40))//28
    
        self.addSubview(cursorView)
        let viewMain = UIView(frame: CGRect(x: 5, y: 0, width: 135, height: 30))
        self.addSubview(viewMain)
        viewMain.layer.cornerRadius = 4
        viewMain.clipsToBounds = true
        viewMain.backgroundColor = MOCColor.DarkBlue.toUIColor()
        
        
        
        
        let redIcon = UIImageView(image: #imageLiteral(resourceName: "ic_red_marker"))
        redIcon.contentMode = .scaleAspectFit
        viewMain.addSubview(redIcon)
        redIcon.frame = CGRect(x: 12, y: 10, width: 10, height: 10)
        
        let blueIcon = UIImageView(image: #imageLiteral(resourceName: "ic_blue_marker"))
        blueIcon.contentMode = .scaleAspectFit
        viewMain.addSubview(blueIcon)
        blueIcon.frame = CGRect(x: 55, y: 10, width: 10, height: 10)
        
        let greenIcon = UIImageView(image: #imageLiteral(resourceName: "ic_green_marker"))
        greenIcon.contentMode = .scaleAspectFit
        viewMain.addSubview(greenIcon)
        greenIcon.frame = CGRect(x: 100, y: 10, width: 10, height: 10)
        
        
        let textSize = CGSize(width: 30, height: 14)
        let font = UIFont(name: "HelveticaNeue", size: 14)
        
        let redValTex = UILabel()
        redValTex.textColor = UIColor.white
        viewMain.addSubview(redValTex)
        redValTex.text = String(Int(values[0]))
        redValTex.textAlignment = .left
        redValTex.font = font
        redValTex.frame = CGRect(origin: CGPoint(x: 27, y: 7), size: textSize)
        
        let blueValTex = UILabel()
        blueValTex.textColor = UIColor.white
        viewMain.addSubview(blueValTex)
        blueValTex.text = String(Int(values[1]))
        blueValTex.textAlignment = .left
        blueValTex.font = font
        blueValTex.frame = CGRect(origin: CGPoint(x: 71, y: 7), size: textSize)
        
        let greenValTex = UILabel()
        greenValTex.textColor = UIColor.white
        viewMain.addSubview(greenValTex)
        greenValTex.text = String(Int(values[2]))
        greenValTex.textAlignment = .left
        greenValTex.font = font
        greenValTex.frame = CGRect(origin: CGPoint(x: 115, y: 7), size: textSize)       
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func imageView(pading: CGFloat, pointDown: Bool?) -> UIImage {
        cursorView.frame = CGRect(x: 57.5 + 5 , y: 12, width: 20, height: 20)
        cursorView.frame.origin.x = cursorView.frame.origin.x - pading
        cursorView.layer.cornerRadius = 2
        cursorView.backgroundColor = MOCColor.DarkBlue.toUIColor()
        cursorView.clipsToBounds = true
        cursorView.transform = cursorView.transform.rotated(by: .pi/4)
        cursorView.dropShadow()
        self.dropShadow()
        return UIImage(view: self)
    }
        
    
}


