//
//  ChartPopupView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 05.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation


class SinglePersentValuePopupView: UIView, ChartMarkerViewProtocol {
    
    
    
    
    let cursorView = UIView()
    
    required init(values : [Double]) {
        super.init(frame: CGRect(x: 0, y: 0, width: 60, height: 50))//28
        
        self.addSubview(cursorView)
        let viewMain = UIView(frame: CGRect(x: 5, y: 11, width: 50, height: 28))
        self.addSubview(viewMain)
        viewMain.layer.cornerRadius = 4
        viewMain.clipsToBounds = true
        viewMain.backgroundColor = MOCColor.DarkBlue.toUIColor()

        
        let redValTex = UILabel()
        redValTex.textColor = UIColor.white
        viewMain.addSubview(redValTex)
        
        let roundedVal = Float(values[0]).rounded(toPlaces: 1)
        if roundedVal == Float(Int(roundedVal))  {
           redValTex.text = String(abs(Int(roundedVal))) + " %"
        }else{
            redValTex.text = String(abs(roundedVal)) + " %"
        }
        
        redValTex.textAlignment = .center
        redValTex.font = UIFont(name: "HelveticaNeue", size: 14)
        redValTex.frame = viewMain.bounds
  
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func imageView(pading: CGFloat, pointDown: Bool?) -> UIImage {
        cursorView.frame = CGRect(x: 21, y: pointDown! ? 23 : 7, width: 20, height: 20)

        cursorView.frame.origin.x = cursorView.frame.origin.x - pading
        cursorView.layer.cornerRadius = 2
        cursorView.backgroundColor = MOCColor.DarkBlue.toUIColor()
        cursorView.clipsToBounds = true
        cursorView.transform = cursorView.transform.rotated(by: .pi/4)
        cursorView.dropShadow()
        self.dropShadow()
        return UIImage(view: self)
    }
    
    
}

