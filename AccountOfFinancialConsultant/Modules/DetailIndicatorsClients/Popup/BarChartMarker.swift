//
//  BarChartMarker.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 05.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import Charts

class BarChartMarker: IMarker {
    
    var view : ChartMarkerViewProtocol? = nil
    
    let popupType : ChartType
    
    var pointDown : Bool = true
    
    var negativValue : Double = 0
    
    required init(type : ChartType) {
        self.popupType = type
    }
    
    var offset: CGPoint = CGPoint(x: 0, y: 0 )
    
    func offsetForDrawing(atPoint: CGPoint) -> CGPoint {
        return atPoint
    }
    
    func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        let values :BarChartDataEntry = entry as! BarChartDataEntry
        
        guard let yValues = values.yValues else { return }
        
        switch self.popupType {
        case .clients:
            view = BarChartPopupView.init(values: yValues)
        case .reject :
            view = SinglePersentValuePopupView.init(values: [values.yValues![highlight.stackIndex]])
            negativValue = highlight.stackIndex == 1  ? values.yValues![highlight.stackIndex] : 0
            pointDown = highlight.stackIndex == 0  ? true : false
            
        case .indicators : break
        }
 
    }
    
    func draw(context: CGContext, point: CGPoint) {
        
        let scale = UIScreen.main.scale
        
        
        
        let widthCtx = CGFloat(context.width)/scale
        let heightCtx = CGFloat(context.height)/scale
        let widthPopup = (view as! UIView).frame.width
        
        var newPoint = point
        newPoint.x = point.x - widthPopup/2.0
        
        if pointDown{
            newPoint.y = point.y - ((view as! UIView).frame.height)
        }else{
            newPoint.y = point.y +  (heightCtx - point.y)*CGFloat(abs(negativValue))/100 + 20
        }
        
        var pading = Double(widthCtx) - Double(point.x+widthPopup/2.0)
        if  pading < 0{
            newPoint.x = newPoint.x + CGFloat(pading)
        }else if Double(pading + Double(widthPopup)) > Double(widthCtx){
            let  rightPading = Double(pading + Double(widthPopup)) - Double(widthCtx)
            newPoint.x = newPoint.x + CGFloat(rightPading)
            pading = rightPading
        }else{
            pading = 0
        }
        
        print(point, newPoint)
        guard let image = view?.imageView(pading: CGFloat(pading), pointDown: pointDown) else { return }
        image.draw(at: newPoint)
        UIGraphicsEndImageContext()
        
    }
}
