//
//  TaskDetailViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TaskDetailViewController: UIViewController, TaskDetailViewProtocol {
    
    

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    @IBOutlet weak var statusView: StatusView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: BigTaskRoundButton!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    var presenter: TaskDetailPresenterProtocol!

    let configurator: TaskDetailConfiguratorProtocol = TaskDetailConfigurator()

    // MARK: - Lifecycle methods
    
    func configure(delegate : TaskDetailOutputModuleProtocol) {

        configurator.configure(with: self)
        presenter.configure(outputMudule: delegate) 
    }
    
	override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.configureView()
        
        statusView.align = .right
    }
    
    // MARK: - Action methods
    
    @IBAction func pessButton(_ sender: Any) {
        presenter.completeTask()
    }
    
    // MARK: - ViewProtocol methods
    
    func presentTaskDetail(task: TaskModel) {
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "d MMMM"
        
        dateLabel.text = dateFormatterPrint.string(from: task.createdAt!)
        textLabel.text = task.specification
        switch task.priority {
        case 1:
            statusView.status = StatusTaskEnum.high
        case 2:
            statusView.status = StatusTaskEnum.middle
        case 3:
            statusView.status = StatusTaskEnum.low
        default:
            break
        }
    }
    
    func showActivity(){
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivity(){
        activityIndicator.stopAnimating()
    }
    
    func taskType(completed: Bool) {
        if completed {
            constraintBottom.constant = 10
            button.isHidden = true
        }else{
            constraintBottom.constant = 100
            button.isHidden = false
        }
        
    }
}
