//
//  TaskDetailProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol TaskDetailConfiguratorProtocol: class {
    func configure(with viewController: TaskDetailViewProtocol)
}

//MARK: OutputModule
protocol TaskDetailOutputModuleProtocol: class {
    
    func apllySelectedTask() -> TaskModel
}

//MARK: Presenter -
protocol TaskDetailPresenterProtocol: class {

    var interactor: TaskDetailInteractorInputProtocol? { get set }
    func configure(outputMudule : TaskDetailOutputModuleProtocol)
    func configureView()
    func completeTask()
}

//MARK: Interactor -
protocol TaskDetailInteractorOutputProtocol: class {
    func selectedTask() ->TaskModel
    func succesful(task : TaskModel)
    func fail(error : String)
    /* Interactor -> Presenter */
}

protocol TaskDetailInteractorInputProtocol: class {

    var presenter: TaskDetailInteractorOutputProtocol?  { get set }
    func loadTaskDetail(taskId : Int)
    func completeTask()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol TaskDetailViewProtocol: class {

    var presenter: TaskDetailPresenterProtocol!  { get set }
    func configure(delegate : TaskDetailOutputModuleProtocol)
    func presentTaskDetail(task : TaskModel)
    func showActivity()
    func hideActivity()
    func taskType(completed : Bool)
    /* Presenter -> ViewController */
}
