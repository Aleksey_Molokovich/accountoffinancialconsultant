//
//  TaskDetailPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TaskDetailPresenter: TaskDetailPresenterProtocol, TaskDetailInteractorOutputProtocol {
    
    
    weak var outputMudule: TaskDetailOutputModuleProtocol?
    
    weak private var view: TaskDetailViewProtocol?
    
    var interactor: TaskDetailInteractorInputProtocol?

    var task : TaskModel?
    
    init(view: TaskDetailViewProtocol) {
        self.view = view
    }
    
    func configure(outputMudule: TaskDetailOutputModuleProtocol) {
        self.outputMudule = outputMudule
        task = (self.outputMudule?.apllySelectedTask())!
    }
    
    func configureView() {
        view?.showActivity()
        
        view?.taskType(completed: task?.status == 0 ? false : true)
        interactor?.loadTaskDetail(taskId: (task?.id)!)
    }
    
    func completeTask() {
        view?.showActivity()
        interactor?.completeTask()
    }
    
    func selectedTask() -> TaskModel {
        return task!
    }
    
    //MARK: InteractorOutputProtocol -
    
    func succesful(task: TaskModel) {
        DispatchQueue.main.async {[unowned self] in
            self.view?.hideActivity()
            self.view?.taskType(completed: task.status == 0 ? false : true)
            self.view?.presentTaskDetail(task: task)
        }
    }
    
    func fail(error: String) {
        DispatchQueue.main.async {[unowned self] in
            self.view?.hideActivity()
            self.view?.presentTaskDetail(task: self.task!)
        }
    }
}
