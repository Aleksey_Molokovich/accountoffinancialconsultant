//
//  TaskDetailConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class TaskDetailConfigurator: TaskDetailConfiguratorProtocol {

    func configure(with viewController: TaskDetailViewProtocol) {
        let presenter = TaskDetailPresenter(view: viewController)
        let interactor = TaskDetailInteractor(presenter: presenter)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
