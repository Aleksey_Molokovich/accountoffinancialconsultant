//
//  TaskDetailInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import ObjectMapper

class TaskDetailInteractor: TaskDetailInteractorInputProtocol {

    

    weak var presenter: TaskDetailInteractorOutputProtocol?
    
    var operation:Operation?
    
    var task : TaskModel?
    
    required init(presenter: TaskDetailInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func loadTaskDetail(taskId : Int) {
        task = presenter?.selectedTask()
        self.presenter?.succesful(task: task!)
//        #if DEBUGMOCK
//            readFromFile(id: taskId)
//            return
//        #endif
//
//        operation = API.task(id:taskId, completion: {[weak self] (json) in
//            guard let sSelf = self else { return }
//            sSelf.task = Mapper<TaskModel>().map(JSON: json as! [String : Any])!
//            sSelf.presenter?.succesful(task: sSelf.task!)
//        }) {[weak self] (error) in
//            guard let sSelf = self else { return }
//            sSelf.presenter?.fail(error: "")
//        }
//
//        guard operation != nil else { return }
//
//        Services.shared.httpOperationService.addOperation(operation!)
    }

    func completeTask() {
        task = presenter?.selectedTask()
        operation = API.closeTask(id:(self.task?.id)!, completion: {[weak self] (json) in
            guard let sSelf = self else { return }
            

            sSelf.task?.status = 1
            
            let dao = Services.shared.dao
            
            dao.addOrUpdateTask(item: sSelf.task!) { [weak self] (Succes, Err) in
                guard let sSelf = self else { return }
                sSelf.presenter?.succesful(task: sSelf.task!)
            }
            
        }) {[weak self] (error) in
            guard let sSelf = self else { return }
            let code = error.statusCode()
            if code == 204{
                let dao = Services.shared.dao
                sSelf.task?.status = 1
                
                dao.addOrUpdateTask(item: sSelf.task!) { [weak self] (Succes, Err) in
                    guard let sSelf = self else { return }
                    sSelf.presenter?.succesful(task: sSelf.task!)
                }
                return
            }
            
            
            sSelf.presenter?.fail(error: "")
        }
        
        guard operation != nil else { return }
        
        Services.shared.httpOperationService.addOperation(operation!)
    }
    
//    func readFromFile(id:Int) {
//
//        let path = Bundle.main.path(forResource: "Tasks", ofType: "json")
//
//        do {
//            let fileUrl = URL(fileURLWithPath: path!)
//            let jsonData = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
//            let json = try! JSONSerialization.jsonObject(with: jsonData)
//            let tasks = Mapper<TaskModel>().mapArray(JSONObject: json)!
//            let task = tasks.filter({$0.id == id}).first
//            self.presenter?.succesful(task: task!)
//        } catch {
//
//        }
//    }
}
