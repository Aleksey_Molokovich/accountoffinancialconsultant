//
//  ChatPresenter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatPresenter: ChatPresenterProtocol, ChatInteractorOutputProtocol {
    

    weak var outputMudule : ChatOutputModuleProtocol?
    
    weak private var view: ChatViewProtocol?
    var interactor: ChatInteractorInputProtocol?
    var router: ChatRouterProtocol!
    
    
    func configure(outputMudule: ChatOutputModuleProtocol) {
        self.outputMudule = outputMudule
        self.interactor?.chatId = outputMudule.chatConfig().chatId
        self.interactor?.url = outputMudule.chatConfig().url
    }
    
    init(view: ChatViewProtocol) {
        self.view = view
    }

    func configureView() {
        interactor?.setupSocket()
    }
    
    //MARK: InteractorOutputProtocol -
    
    func connectTo(storage: CBMessageStorage) {
        view?.connectTo(storage: storage)
    }
    
    func succesfulRecivedMessage(message:CBMessage){
        view?.hideActivity()
    }
    
    func fail(error: String) {
        view?.hideActivity()
    }
    
    
}
