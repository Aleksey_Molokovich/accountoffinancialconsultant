//
//  ChatProtocols.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import Foundation
//MARK: Configurator -
protocol ChatConfiguratorProtocol: class {
    func configure(with viewController: ChatViewProtocol)
}

//MARK: OutputModule
protocol ChatOutputModuleProtocol: class {
    
    func chatConfig() -> (chatId : Int, url : String)
}

//MARK: Router -
protocol ChatRouterProtocol: class {
    func closeCurrentViewController()
}
//MARK: Presenter -
protocol ChatPresenterProtocol: class {

    var interactor: ChatInteractorInputProtocol? { get set }
    var router: ChatRouterProtocol! { set get }
    func configure(outputMudule: ChatOutputModuleProtocol)
    func configureView()
}

//MARK: Interactor -
protocol ChatInteractorOutputProtocol: class {
    func succesfulRecivedMessage(message:CBMessage)
    func fail(error : String)
    func connectTo(storage : CBMessageStorage)
    /* Interactor -> Presenter */
}

protocol ChatInteractorInputProtocol: class {

    var presenter: ChatInteractorOutputProtocol?  { get set }
    var url : String?   { get set }
    var chatId: Int?  { get set }
    func setupSocket()
    /* Presenter -> Interactor */
}

//MARK: View -
protocol ChatViewProtocol: class {

    var presenter: ChatPresenterProtocol!  { get set }
    func connectTo(storage : CBMessageStorage)
    func showActivity()
    func hideActivity()
    /* Presenter -> ViewController */
}
