//
//  ChatConfigurator.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatConfigurator: ChatConfiguratorProtocol {

    func configure(with viewController: ChatViewProtocol) {
        let presenter = ChatPresenter(view: viewController)
        let interactor = ChatInteractor(presenter: presenter)
        let router = ChatRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
