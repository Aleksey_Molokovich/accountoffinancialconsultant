//
//  ChatViewController.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatViewController: UIViewController, ChatViewProtocol {
    
    
    

	var presenter: ChatPresenterProtocol!

    let configurator: ChatConfiguratorProtocol = ChatConfigurator()

    let attachButton = UIButton()
    
    var leftPaddingView = UIView()
    
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    
    
    @IBOutlet weak var chat: CBChatView!
    
    // MARK: - Lifecycle methods
    
    func configure(outputMudule : ChatOutputModuleProtocol) {
        
        configurator.configure(with: self)
        presenter.configure(outputMudule: outputMudule)
    }
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.configureView()
        
        chat.bottomHeight = 55
        
        self.attachButton.setImage(UIImage.init(named: "ic_attach"), for: .normal)
        attachButton.frame = CGRect(x: 16, y: 10, width: 35, height: 35)
        attachButton.layer.cornerRadius = 4
        attachButton.clipsToBounds = true
        attachButton.backgroundColor = MOCColor.Blue.toUIColor()

        leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 55))
//        leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 55))
//        leftPaddingView.addSubview(attachButton)
        
        let bottomView = chat.bottomView
        
        
        bottomView?.backgroundColor = MOCColor.DarkBlue.toUIColor()
        bottomView?.leftViewMode = .always
        bottomView?.buttonSize = CGSize.init(width: 35, height: 35)
        bottomView?.textSize = CGSize.init(width: 0, height: 35)
        bottomView?.leftPaddingView = leftPaddingView
        bottomView?.placeholderColor = UIColor.init(hexString: "#96A6A7")
        bottomView?.textViewBackgroundColor = UIColor.white
        bottomView?.font = UIFont.systemFont(ofSize: 13)
        bottomView?.setButtonImage(UIImage.init(named: "ic_send_message"), for: .normal)
        
        bottomView?.textViewCornerRadius = 7
        
        chat.registrateMessagePrototypeNib(UINib(nibName: "StaffMessagePrototype", bundle: Bundle.main), forKey: CBChatStaffMessagePrototype)
        chat.registrateMessagePrototypeNib(UINib(nibName: "UserMessagePrototype", bundle: Bundle.main), forKey: CBChatUserMessagePrototype)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
//        let ms = CBMessage()
//        ms.text = "И — пожалуйста! — никаких химических кетчупов и уж тем более майонезов. Надо наслаждаться вкусом мяса как такового, не надо его ни чем заглушать. Овощи, свежий хлеб, соль, перец — этого в качестве аккомпанемента вполне достаточно."
//        ms.isStaff = true
//        ms.isReaded = true
//        ms.time = Date.init().timeIntervalSince1970
//
//
//        let mu = CBMessage()
//        mu.text = "Николай, пожайлуста, пришлите скриншот от экарна чата на sovcommobile@gmail.com"
//        mu.isStaff = false
//        ms.isReaded = true
//        mu.time = Date.init().timeIntervalSince1970
//
//
//        let ms2 = CBMessage()
//        ms2.isStaff = false
//        let path = Bundle.main.path(forResource: "bg", ofType: "png")
//        ms2.filePath = path
//        ms2.time = Date.init().timeIntervalSince1970
//
//
//        storage.addMessages([ms,mu,ms2])
//        chat.messageStorage = storage
        
    }
    
//    @objc func keyboardWillShow(notification: NSNotification) {
//        constraintBottom.constant -= (self.tabBarController?.tabBar.frame.height)!
//    }
//    
//    @objc func keyboardWillHide(notification: NSNotification){
//        constraintBottom.constant = 0
//    }
    
    // MARK: - Action methods
    

    // MARK: - ViewProtocol methods
    
    func connectTo(storage: CBMessageStorage) {
        chat.messageStorage = storage
    }
    
    func showActivity(){
    }
    
    func hideActivity(){
    }
    
    //MARK: CBBottomDelegate
    

    
    
}
