//
//  ChatToolBar.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

enum CBBottomViewMode {
    case CBBottomViewModeNever
    case CBBottomViewModeWhileEditing
    case CBBottomViewModeUnlessEditing
    case CBBottomViewModeAlways
}

protocol ChatToolBarProtocol {
    func bottom(view : ChatToolBar, didChangeText : String )
    func bottomViewDidSendAction (view : ChatToolBar)
}


class ChatToolBar: UIView, UITextViewDelegate {

//    weak var delegate : ChatToolBarProtocol?
    
    var textColor = UIColor.init(hexString: "#2C3E50")
    var placeholderColor = UIColor.init(hexString: "#96A6A7")
    var textViewBackgroundColor = UIColor.init(hexString: "#F6F8F9")
    var font = UIFont.systemFont(ofSize: 14)
    
    var buttonSize = CGSize.init(width: 10, height: 10)
    var textSize = CGSize.init(width: 10, height: 10)
//    var leftPaddingView : UIView
//    var rightPaddingView : UIView
    var rightViewMode = CBBottomViewMode.CBBottomViewModeNever
    var leftViewMode = CBBottomViewMode.CBBottomViewModeNever
//    var textViewCornerRadius : CGFloat
    
    private var textView = UITextView()
    private var sendButton = UIButton()
    private var viewContainer = UIView()
    
    private var rightViewPlaceholder = UIView()
    private var leftViewPlaceholder = UIView()
    private var sendButtonWidth = NSLayoutConstraint()
    private var sendButtonHeight = NSLayoutConstraint()
    private var textViewHeight = NSLayoutConstraint()
    private var rightViewPlaceholderWidth = NSLayoutConstraint()
    private var leftViewPlaceholderWidth = NSLayoutConstraint()


    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor.init(hexString: "#2C3E50")
        
//        leftViewMode = .CBBottomViewModeNever
//        rightViewMode = .CBBottomViewModeNever
//        
//        leftViewPlaceholder.backgroundColor = UIColor.blue
//        leftViewPlaceholder.translatesAutoresizingMaskIntoConstraints = false
//        self.addSubview(leftViewPlaceholder)
//
//        NSLayoutConstraint.activate([leftViewPlaceholder.widthAnchor.constraint(equalToConstant: 35),
//                                     leftViewPlaceholder.topAnchor.constraint(equalTo: self.topAnchor),
//                                     leftViewPlaceholder.bottomAnchor.constraint(equalTo: self.bottomAnchor),
//                                     leftViewPlaceholder.leadingAnchor.constraint(equalTo: self.leadingAnchor)])
        
        textView.backgroundColor = UIColor.white
        
        textView.text = "Написать сообщение"
        textView.textColor = UIColor.init(hexString: "#8B8C9F")
        textView.font = UIFont.systemFont(ofSize: 14)
        
        textView.translatesAutoresizingMaskIntoConstraints = false;
        textView.delegate = self;
        
        self.addSubview(textView)
        

        let sendButton = UIButton.init(type: .system)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.isEnabled = false
        sendButton.titleLabel?.text = "asda"
        sendButton.tintColor = UIColor.white
        sendButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        sendButton.contentHorizontalAlignment = .center
        sendButton.contentVerticalAlignment = .center
        sendButton.backgroundColor = UIColor.red
        sendButton.setTitleColor(UIColor.init(hexString: "#F7A914"), for: .normal)
        sendButton.setTitleColor(UIColor.init(hexString: "#8B8C9F"), for: .disabled)

        sendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)

        self.addSubview(sendButton)

        NSLayoutConstraint.activate([sendButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     sendButton.leadingAnchor.constraint(equalTo: textView.trailingAnchor, constant: 10),
                                     sendButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
                                     sendButton.heightAnchor.constraint(equalToConstant: 35),
                                     sendButton.widthAnchor.constraint(equalToConstant: 100)])        
        
        
        self.addSubview(viewContainer)
        
        viewContainer.backgroundColor = UIColor.white
        viewContainer.layer.cornerRadius = 4
        viewContainer.clipsToBounds = true
        viewContainer.translatesAutoresizingMaskIntoConstraints = false;
        NSLayoutConstraint.activate([viewContainer.heightAnchor.constraint(equalToConstant: 35),
                                     viewContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     viewContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 30),
                                     viewContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30)])
        
        viewContainer.addSubview(textView)
        NSLayoutConstraint.activate([textView.heightAnchor.constraint(equalTo: viewContainer.heightAnchor, multiplier: 1),
                                     textView.centerYAnchor.constraint(equalTo: viewContainer.centerYAnchor),
                                     textView.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 0),
                                     textView.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: -30)])
        
        
    }
    
    @objc func sendAction(){
        
    }
    
    //MARK: TextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(hexString: "#8B8C9F") {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Написать сообщение"
            textView.textColor = UIColor.init(hexString: "#8B8C9F")
        }
    }

}
























