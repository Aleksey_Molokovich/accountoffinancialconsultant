//
//  ChatRouter.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit

class ChatRouter: ChatRouterProtocol {

    weak var viewController: UIViewController?

    init(viewController: ChatViewProtocol) {
        self.viewController = viewController as? UIViewController
    }
    

    
    func closeCurrentViewController() {
    }
}
