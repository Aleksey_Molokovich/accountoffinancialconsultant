//
//  ChatInteractor.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 26.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
//
//

import UIKit
import Starscream
import ObjectMapper

class ChatInteractor:NSObject, CBMessageStorageDelegate, ChatInteractorInputProtocol, WebSocketDelegate {
    
    var url: String?
    
    var chatId: Int?
    
    
    let userPhone = "+7" + Services.shared.accountService.phone
    let userFIO =  Services.shared.accountService.userFIO
    
    let storage = CBMessageStorage()
    
    var socket : WebSocket?
    
    let dao = Services.shared.dao
    
    weak var presenter: ChatInteractorOutputProtocol?
    
    required init(presenter: ChatInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    
    func setupSocket(){
        #if DEBUGMOCK
        socket = WebSocket(url: URL(string: url! )!, protocols: ["chat"])
        
        #else
        socket = WebSocket(url: URL(string: url!)!, protocols: ["chat"])
        #endif
        
        guard let _ = socket else { return  }
        
        self.storage.delegate = self
        presenter?.connectTo(storage: self.storage)
        socket?.delegate = self
        socket?.connect()
        self.loadMessageFromBase()
    }
    
    func websocketDidConnect(socket: WebSocketClient) {
        let dict = [
            "os":"iOS",
            "device_token":"",
            "phone":userPhone,
            "cred":userFIO
        ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            socket.write(data: data)
        } catch  {
            print("websocketDidConnect ERROR")
        }
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        
        var messages = Mapper<MessageModel>().mapArray(JSONString: text)!
        messages = messages.filter( {$0.date != nil })
        

        for (index, message) in messages.enumerated() {
            message.chatId = chatId!
            dao.addOrUpdateMessage(item: message) { (result, error) in

                if index >= messages.count - 1
               {
                    self.loadMessageFromBase()
                }
                
            }
        }
        
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
    
    func messageStorageAddNewMessage(_ message: CBMessageProtocol!) {
        
        guard let _ = message.text, message.text.count > 0 else { return }
        
        let jsonString = [
            "text":message.text,
            "to":"Staff",
            "status":0,
            "from": userPhone,
            "date": Int64(message.time)
            ] as [String : Any]
        
        
        
        do {
            let data = try JSONSerialization.data(withJSONObject: jsonString, options: .prettyPrinted)
            
            let model = Mapper<MessageModel>().map(JSON: jsonString)
            model?.text = message.text
            model?.date = message.time
            model?.chatId = chatId
            dao.addOrUpdateMessage(item: model!) { (result, error) in
            }
            
            socket?.write(data: data)
        } catch  {
            print("websocketDidConnect ERROR")
        }
    }
    
    func loadMessageFromBase(){
        dao.messages { (messages, error) in
            
            for message in messages! {
                
                let ms = CBMessage()
                ms.text = message.text
                if message.role == "staff" {
                    ms.isStaff = true
                }else{
                    ms.isStaff = false
                }
                
                if message.date != nil{
                    ms.time = message.date!
                }
                print(message.hash())
                
               self.storage.add(ms)
            }
        }
    }

}
