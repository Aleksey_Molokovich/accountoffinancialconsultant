//
//  StaffMessagePrototype.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 29.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class StaffMessagePrototype: UIView, CBPrototype {
    @IBOutlet weak var textViewPlaceHolderBottomCorner: UIView!
    @IBOutlet weak var textViewPlaceholder: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var fileImageView: UIImageView!
    @IBOutlet weak var markImageView: UIImageView!
    @IBOutlet weak var markImageViewWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textViewPlaceholder.backgroundColor = UIColor.init(hexString: "#ECF0F1")
        self.textViewPlaceholder.layer.cornerRadius = 7
        self.textViewPlaceholder.clipsToBounds = true
        self.textViewPlaceHolderBottomCorner.backgroundColor = UIColor.init(hexString: "#ECF0F1")
        self.backgroundColor = UIColor.clear
    }
    
    //MARK: - CBPrototype protocol methods
    
    func getMessageLabel() ->UILabel{
        return self.textLabel
    }
    
    func preferredPrototypeMinSize() -> CGSize{
        return CGSize(width: 60, height: 60)
    }
    
    
    func setMessage(_ message: CBMessageProtocol!) {
        
        self.textLabel.text = message.text
        
        let formater = DateFormatter(withFormat: "HH:mm", locale: "ru_RU")
        
        if (message.isReaded)
        {
            let image = UIImage(named: "ic_chat_read_mark")
            self.markImageView.image = image
            self.markImageViewWidth.constant = 0.0
        }
        
        self.timeLabel.text = formater.string(from: Date(timeIntervalSince1970: message.time))
    }
}
