//
//  BigBlueButton.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class BigBlueButton: UIButton {

    /// Corner radius of the background rectangle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        
        self.backgroundColor = MOCColor.Blue.toUIColor()
    }
    
    override var isEnabled: Bool{
        didSet {
            if isEnabled {
                self.backgroundColor = MOCColor.Blue.toUIColor()
            }else{
                self.backgroundColor = UIColor.init(hexString: "#E0E7E8")
            }
        }
    }

}
