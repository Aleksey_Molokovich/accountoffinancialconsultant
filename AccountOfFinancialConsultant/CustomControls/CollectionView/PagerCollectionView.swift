//
//  PagerCollectionView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 17.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

protocol PagerCollectionViewDelegate : class {
    func pager(_ pager : PagerCollectionView , indexPath : IndexPath) -> UICollectionViewCell
    func pageWillDisplay(indexPath : IndexPath)
    func pager(_ pager: PagerCollectionView, didSeselectItemAt indexPath: IndexPath)
}

class PagerCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    var count : Int = 0
    
    var showItem : Int = 0{
        didSet{
            let index = IndexPath(item: showItem, section: 0)
            self.scrollToItem(at: index , at: [.centeredHorizontally, .centeredVertically], animated: false)
        }
    }

    var flowLayout = UICollectionViewFlowLayout()
    weak var pagerDelegate : PagerCollectionViewDelegate?
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupFlowLayout()
        self.delegate = self
        self.dataSource = self
    }
    
    func setupFlowLayout(){
        flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowLayout.scrollDirection = .horizontal
        self.showsHorizontalScrollIndicator = false
        self.isPagingEnabled = true
        self.collectionViewLayout = flowLayout
        
    }

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let delegate = pagerDelegate else {
            print("Your delegate does not conform to PagerCollectionViewDelegate")
            assert(false)
            return UICollectionViewCell()
        }
        
        return delegate.pager(self, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = pagerDelegate else { return }
        delegate.pager(self, didSeselectItemAt: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let delegate = pagerDelegate else { return }
        let visibleRect = CGRect(origin: self.contentOffset, size: self.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let visibleIndexPath = self.indexPathForItem(at: visiblePoint) else { return }
        delegate.pageWillDisplay(indexPath: visibleIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.frame.size
    }
    
    
}

