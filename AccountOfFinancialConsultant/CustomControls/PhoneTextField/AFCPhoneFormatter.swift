//
//  AFCPhoneFormatter.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class AFCPhoneFormatter {

    var prefix : String
    var mask : String
    
    init(prefix : String, mask: String) {
            
        self.prefix = prefix
        self.mask = mask
    }
    
    func formattedText(text : String, inRange  range : NSRange, replacementString : String) -> String {
        if (prefix.count>0 && range.location<prefix.count) {
            return text
        }
        
        var result = text
        result = result.replacingCharacters(in: Range(range, in: text)!, with: replacementString)
        
        if mask.count>0{
            return formatted(string: result)
        }
        
        return result
    }
    

    func deformattedString(string : String) -> String {
        var result = string
        
        if prefix.count>0{
            result = String(result[...prefix.endIndex])
        }
        
        if mask.count>0{
            result = result.filter { "0123456789".contains($0) }
        }
        
        return result
    }
    
    func formatted(string : String) -> String {

        var formattedString = string
        if (prefix.count > 0 &&
            string.count>=prefix.count &&
            String(string[...prefix.endIndex]) == prefix){
            formattedString = String(string[prefix.endIndex...])
        }
        
        formattedString = string.filter { "0123456789".contains($0) }
        
        var characterIndex = 0
        var result : String = ""
        
        for i in 0..<mask.count{
            if (characterIndex >= formattedString.count){
                break
            }
            
            let index = String.Index.init(encodedOffset: i)
            if (mask[index] == "#"){
                let nextIndex =  String.Index.init(encodedOffset: characterIndex)
                result.append(formattedString[nextIndex])
                characterIndex = characterIndex+1
            }else{
                result.append(mask[index])
            }
        }
        
        if prefix.count>0 {
            return prefix+result
        }
        
        return result
    }
}

