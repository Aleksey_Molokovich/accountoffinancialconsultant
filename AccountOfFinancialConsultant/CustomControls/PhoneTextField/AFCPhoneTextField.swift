//
//  AFCPhoneTextField.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

@objcMembers class AFCPhoneTextField: UITextField, UITextFieldDelegate{

    let formatter : AFCPhoneFormatter = AFCPhoneFormatter.init(prefix: "", mask: "(###) ###-##-##")
    
    @objc dynamic var deformattedText : String?
    
    override var text: String?{
        didSet{
            let str = formatter.deformattedString(string: self.text!)
            
            if str.count < 10 {
                self.deformattedText = nil
                return
            }
            
            self.deformattedText = str
            
        }
    }
    
    private func initialize() {
        delegate = self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        if ((textField.text?.count)! + 1 > 15 &&
            range.length != 1)
        {
            return false
        }
        
        let prevText = textField.text
        var formattedText = formatter.formattedText(text: textField.text!, inRange: range, replacementString: string)
        
        if prevText == formattedText{
            formattedText = formatter.formatted(string: prevText!)
        }
        
        textField.text =  formattedText
        
        
        //correct cursor position
        var correctRange = range
        
        if string != ""{
            correctRange.location = correctRange.location + formattedText.count - (prevText?.count)!
        }
        
        let start = textField.position(from: textField.beginningOfDocument, offset: correctRange.location)
        if start == nil {
            return false
        }
        
        let end = textField.position(from: start!, offset: 0)
        textField.selectedTextRange = textField.textRange(from: start!, to: end!)
        
        return false
    }
    


}
