//
//  BigTaskRoundButton.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class BigTaskRoundButton: UIButton {

    /// Corner radius of the background rectangle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        self.layer.borderColor = MOCColor.Blue.toUIColor().cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(MOCColor.Blue.toUIColor(), for: .normal)
        self.backgroundColor = UIColor.clear
    }

}
