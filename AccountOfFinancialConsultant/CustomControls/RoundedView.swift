//
//  RoundWhiteView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 20.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 7
        self.clipsToBounds = true
        self.layer.borderWidth = 0
        self.backgroundColor = UIColor.white
    }
}
