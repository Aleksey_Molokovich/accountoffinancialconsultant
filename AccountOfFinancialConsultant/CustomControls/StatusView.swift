//
//  StatusView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

enum StatusTaskEnum {
    case high
    case middle
    case low
}

enum StatusTaskAlignEnum {
    case left
    case right
}

class StatusView: UIView {

    var status : StatusTaskEnum?{
        set{
            guard let value = newValue else { return }
            switch value {
            case .low:
                point.backgroundColor = MOCColor.Green.toUIColor()
                label.text = "Низкий приоритет"
                label.textColor = MOCColor.Green.toUIColor()
            case .middle:
                point.backgroundColor = MOCColor.Blue.toUIColor()
                label.text = "Средний приоритет"
                label.textColor = MOCColor.Blue.toUIColor()
            case .high:
                point.backgroundColor = MOCColor.Red.toUIColor()
                label.text = "Высокий приоритет"
                label.textColor = MOCColor.Red.toUIColor()
            }
        }
        get{
            return self.status
        }
    }
    
    var align : StatusTaskAlignEnum?{
        set{
            guard let value = newValue else { return }
            switch value {
            case .left:
                label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30).isActive = true
                label.textAlignment = .left
            case .right:
                label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
                label.textAlignment = .right
            }
        }
        get{
            return self.align
        }
    }
    
    let label = UILabel()
    
    let point = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.addSubview(point)
        self.addSubview(label)
        
        point.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        point.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        point.leftAnchor.constraint(equalTo: label.leftAnchor, constant: -15).isActive = true
        point.heightAnchor.constraint(equalToConstant: 8).isActive = true
        point.widthAnchor.constraint(equalToConstant: 8).isActive = true

        label.numberOfLines = 1
        label.font =  UIFont(name: "HelveticaNeue-Light", size: 13)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        point.layer.cornerRadius = point.frame.height/2
        point.clipsToBounds = true
    }

}
