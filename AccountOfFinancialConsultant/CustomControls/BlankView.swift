//
//  BlankView.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

enum BlankTypeEnum {
    case noOpenTasks, noClosedTasks, noChartsData, some
}

class BlankView: UIView {

    var blankType : BlankTypeEnum = .some
    {
        didSet{
            switch blankType {
            case .some:
                label.text = ""
            case .noOpenTasks:
                label.text = "В данный момент нет\nактивных задач"
            case .noClosedTasks:
                label.text = "В данный момент нет\nзакрытых задач"
            case .noChartsData:
                label.text = "Информация временно\nнедоступна"
            }
        }
    }
    let label = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor(hexString: "#ECF0F1")
        
        let image = UIImageView(image: #imageLiteral(resourceName: "img_error_gray"))
        self.addSubview(image)
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor, constant : self.frame.height * 0.25).isActive = true
        image.heightAnchor.constraint(equalToConstant: 60).isActive = true
        image.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "#98A4A9")
//        "SFProText-Light", "SFProText-Regular", "SFProText-Semibold"
        label.font = UIFont(name: "SFProText-Regular", size: 18)
        label.topAnchor.constraint(equalTo: image.bottomAnchor, constant : 20).isActive = true
        label.widthAnchor.constraint(equalToConstant: self.frame.width).isActive = true
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        
    }
}
