//
//  CoreDataDAO.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 21.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//
import Foundation
import CoreStore
import ObjectMapper

public typealias RepoCallback<T> = (_ result: T?, _ error: Error?) -> Void

class CoreDataDAO {
    
    func tasks(callback: @escaping RepoCallback<[TaskModel]>) {
        var tasks: [TaskModel] = []

        CoreStore.perform(asynchronous: { transaction in
            let items = transaction.fetchAll(From<TaskCD>())

            if let items = items {
                tasks = items.map({ (item : TaskCD) -> TaskModel in
                    
                    return TaskMapper.map(from: item)
                })
            }
        }, completion: { result in
            switch result {
            case .success:
                callback(tasks, nil)
            case .failure(let error):
                callback(nil, error)
            }
        })
    }
    
    func deleteAllTasks(with callback: @escaping RepoCallback<Bool>) {
        CoreStore.perform(asynchronous: { transaction in
            transaction.deleteAll(From<TaskCD>())
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
    }
    
    func addOrUpdateTask(item: TaskModel, callback: @escaping RepoCallback<Bool>) {
        
        let predicate = NSPredicate(format: "id = %ld", item.id)
        
        CoreStore.perform(asynchronous: { transaction in
            
            var entity = transaction.fetchOne(From<TaskCD>(), Where<TaskCD>(predicate))
            
            if (entity == nil){
                entity = transaction.create(Into<TaskCD>())
            }
            
            TaskMapper.map(from : item, to : entity! )
            
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
        
    }
    
    //MARK: Report
    
    func reports(callback: @escaping RepoCallback<[ReportModel]>) {
        var reports: [ReportModel] = []
        
        CoreStore.perform(asynchronous: { transaction in
            let items = transaction.fetchAll(From<ReportCD>())
            
            if let items = items {
                reports = items.map({ (item : ReportCD) -> ReportModel in
                    return ReportMapper.map (from : item)
                })
            }
        }, completion: { result in
            switch result {
            case .success:
                callback(reports, nil)
            case .failure(let error):
                callback(nil, error)
            }
        })
    }
    
    func deleteAllReports(with callback: @escaping RepoCallback<Bool>) {
        CoreStore.perform(asynchronous: { transaction in
            transaction.deleteAll(From<ReportCD>())
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
    }
    
    func addOrUpdateReport(item: ReportModel, callback: @escaping RepoCallback<Bool>) {
        
        let predicate = NSPredicate(format: "id = %ld", item.id)
        
        CoreStore.perform(asynchronous: { transaction in
            
            var entity = transaction.fetchOne(From<ReportCD>(), Where<ReportCD>(predicate))
            
            if (entity != nil){
                transaction.delete(entity)
            }
            entity = transaction.create(Into<ReportCD>())

            ReportMapper.map(from : item, to : entity! )
            
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
        
    }
    
    //MARK: Message
    
    func messages(callback: @escaping RepoCallback<[MessageModel]>) {
        var messages: [MessageModel] = []
        
        CoreStore.perform(asynchronous: { transaction in
            let items = transaction.fetchAll(From<MessageCD>())
            
            if let items = items {
                messages = items.map({ (item : MessageCD) -> MessageModel in
                    return MessageMapper.map(from: item)
                })
            }
        }, completion: { result in
            switch result {
            case .success:
                callback(messages, nil)
            case .failure(let error):
                callback(nil, error)
            }
        })
    }
    
    func addOrUpdateMessage(item: MessageModel, callback: @escaping RepoCallback<Bool>) {
        
        CoreStore.perform(asynchronous: { transaction in
            let predicate = NSPredicate(format: "md5 = %@", item.hash())
            
            var entity = transaction.fetchOne(From<MessageCD>(), Where<MessageCD>(predicate))
            
            if entity == nil {
                entity = transaction.create(Into<MessageCD>())
            }
            
            MessageMapper.map(from : item, to : entity! )
        
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
        
    }
    
    func deleteAllMessages(with callback: @escaping RepoCallback<Bool>) {
        CoreStore.perform(asynchronous: { transaction in
            transaction.deleteAll(From<MessageCD>())
        }, completion: { result in
            switch result {
            case .success:
                callback(true, nil)
            case .failure(let error):
                callback(false, error)
            }
        })
    }
}
