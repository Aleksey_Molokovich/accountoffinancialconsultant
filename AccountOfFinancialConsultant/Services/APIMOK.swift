//
//  APIMOK.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 04.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
typealias CompletionHandler = (_ result: Any) -> Void
typealias ErrorCompletionHandler = (_ error:NSError) -> Void

enum ReportsFilter{
    case month
    case week
    case all
}

class API  {
    
    class func tasks(completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        guard Services.shared.accountService.sessionUUID != nil else {
            return nil
        }
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/tasks")!)
        request.httpMethod = "GET"
        
        let operation = HTTOperation.init(urlRequest: request, tag: "Tasks");
        
        operation?.opCompletionBlock = { (data, sender) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                completion(json )

            } catch {
                return
            }
        }
        
        operation?.opErrorBlock = { error, sender in
            
            errorBlock(error!)
        }
        
        return operation
    }
    
    class func sendSMSTo(phone : String, completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/auth")!)
        request.httpMethod = "POST"
        do {
            try request.httpBody = JSONSerialization.data(withJSONObject: ["phone" : phone], options: .prettyPrinted)
        } catch  {
            return nil
        }
        
        let operation:HTTOperation = HTTOperation.init(urlRequest: request, tag: "Auth");
        
        operation.opCompletionBlock = {  (data, sender) in
            
            Services.shared.accountService.phone = phone
            
            completion( true )
        }
        
        operation.opErrorBlock = { error, sender in
            errorBlock(error!)
        }
        
        return operation
    }
    
    class func task(id:Int, completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        guard Services.shared.accountService.sessionUUID != nil else {
            return nil
        }
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/tasks/" + String(id))!)
        request.httpMethod = "GET"
        
        let operation = HTTOperation.init(urlRequest: request, tag: "Tasks");
        
        operation?.opCompletionBlock = { (data, sender) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                completion(json )
                
            } catch {
                return
            }
        }
        
        operation?.opErrorBlock = { error, sender in
            
            errorBlock(error!)
        }
        
        return operation
    }
    
    class func closeTask(id:Int, completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        guard Services.shared.accountService.sessionUUID != nil else {
            return nil
        }
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/tasks/" + String(id) + "/complete")!)
        request.httpMethod = "POST"
        
        let operation = HTTOperation.init(urlRequest: request, tag: "Tasks");
        
        operation?.opCompletionBlock = { (data, sender) in
            
            completion(true)
        }
        
        operation?.opErrorBlock = { error, sender in
            
            errorBlock(error!)
        }
        
        return operation
    }
    
    class func reports(filter : ReportsFilter, completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        guard Services.shared.accountService.sessionUUID != nil else {
            return nil
        }
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/reports")!)
        request.httpMethod = "GET"
        
//        var period = ""
//        switch filter {
//        case .week:
//            period = "week"
//        case .month:
//            period = "month"
//        }
        
//        do {
//            try request.httpBody = JSONSerialization.data(withJSONObject: ["period" : period], options: .prettyPrinted)
//        } catch  {
//            return nil
//        }
        
        let operation = HTTOperation.init(urlRequest: request, tag: "Tasks");
        
        operation?.opCompletionBlock = { (data, sender) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                completion(json )
                
            } catch {
                return
            }
        }
        
        operation?.opErrorBlock = { error, sender in
            
            errorBlock(error!)
        }
        
        return operation
    }
    
     class func verifyCode(code:String, completion:@escaping CompletionHandler, errorBlock :@escaping ErrorCompletionHandler ) -> Operation? {
        
        let phone = Services.shared.accountService.phone
        
        var request:URLRequest = URLRequest.init(url: URL.init(string: baseUrl + "/confirm")!)
        request.httpMethod = "POST"
        do {
            try request.httpBody = JSONSerialization.data(withJSONObject: ["phone" : phone, "code" : code], options: .prettyPrinted)
        } catch  {
            return nil
        }
        
        
        let operation:HTTOperation = HTTOperation.init(urlRequest: request, tag: "Auth");
        
        operation.opCompletionBlock = { (data, sender) in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                completion(json )
            } catch {
                return
            }

        }
        
        operation.opErrorBlock = {error, sender in
            
            errorBlock(error!)
        }
        
        return operation
    }
    
}
