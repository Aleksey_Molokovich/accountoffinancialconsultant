//
//  AccountService.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 19.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit

let kAuthorizedUser = "AuthorizedUser"
let kAuthorizedPhone = "AuthorizedPhone"
let kAuthorizedUserFIO = "AuthorizedUserFIO"
let kSessionUUID = "SessionUUID"
let kNotificationConfig = "NotificationConfig"
//let kShouldReportUpdate = "ShouldReportUpdate"

class AccountService {

    var notificationConfig : NotificationConfigModel{
        set{
            do {
                let data = try JSONEncoder().encode(newValue)
                UserDefaults.standard.set(data, forKey: kNotificationConfig)
            } catch  {}
        }
        get{
            var data = NotificationConfigModel()
            guard let json = UserDefaults.standard.data(forKey:kNotificationConfig) else { return data}
            do {
                data = try JSONDecoder().decode(NotificationConfigModel.self, from: json)
            } catch  {}
            
            return data
        }
    }
    
    var phone : String{
        set
        {
            UserDefaults.standard.set(newValue, forKey: kAuthorizedPhone)
        }
        get
        {
            #if DEBUGMOCK
            return "9050392048"
            #endif
            guard let val = UserDefaults.standard.string(forKey: kAuthorizedPhone) else { return ""}
            return val
        }
    }
    
    var userFIO : String{
        set
        {
            UserDefaults.standard.set(newValue, forKey: kAuthorizedUserFIO)
        }
        get
        {
            guard let val = UserDefaults.standard.string(forKey: kAuthorizedUserFIO) else { return ""}
            return val
        }
    }
    
    var sessionUUID : String?{
        set
        {
            UserDefaults.standard.set(newValue, forKey: kSessionUUID)
        }
        get
        {
            #if DEBUGMOCK
            return "571894da-8664-4e37-b559-b3b2a8c49018"
            #endif
            
            return UserDefaults.standard.string(forKey: kSessionUUID)
        }
    }
    
    class func isAuthorized() -> Bool {
        let login = UserDefaults.standard.string(forKey: kAuthorizedUser)
        if (login != nil) {
            return true
        }
        #if DEBUGMOCK
        return true
        #endif
        return false
    }
    
    func succesfullAuthorized(login : String, fio : String, uuid : String)  {
        UserDefaults.standard.set(login, forKey: kAuthorizedUser)
        self.userFIO = fio
        self.sessionUUID = uuid
    }
    
    func clearAuthorizedData() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
