//
//  HTTPOperation.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

public typealias HTTPOperationCompletionBlock = (_ result: Data?, _ sender: Any?) -> Void
public typealias HTTPOperationErrorBlock = (_ error: NSError?, _ sender:Any?) -> Void

let baseUrl = "https://crcal-test.sovcombank.ru/fk/api/v1"

enum kBaseOperationStatus : Int
{
    case kBaseOperationStatus_Nonne = 0
    case kBaseOperationStatus_InProgress
    case kBaseOperationStatus_Done
    case kBaseOperationStatis_Failed
}

enum kHTTPOperationError : Int
{
    case kHTTPOperationErrorHTTP = 1
    case kHTTPOperationErrorInvalidContentType
    case HTTPOperationErrorInvalidContentType
    case HTTPOperationErrorStartup
}

class HTTOperation : Operation
{
    public var tag:String!
    private var opUrl:URL!
    private var opError:NSError?
    public var opRequest:URLRequest!
    private var opDataTask: URLSessionDataTask?
    private var actualResponseBody:Data?
    
    public var opCompletionBlock: HTTPOperationCompletionBlock?
    public var opErrorBlock: HTTPOperationErrorBlock?
    public var opResponse:HTTPURLResponse?
    
    private var _opStatus: kBaseOperationStatus = .kBaseOperationStatus_Nonne
    private var opStatus: kBaseOperationStatus
    {
        set
        {
            let transitionArray: [UInt8] = [
                // None ->
                /* None */ 0, /* InProgress */ 1, /* Done */ 3, /* Failed */ 3,
                           
                           // InProgress ->
                /* None */ 1, /* InProgress */ 0, /* Done */ 3, /* Failed */ 3,
                           
                           // Done ->
                /* None */ 0, /* InProgress */ 3, /* Done */ 0, /* Failed */ 0,
                           
                           // Failed ->
                /* None */ 0, /* InProgress */ 3, /* Done */ 0, /* Failed */ 0,
            ]
            
            let trx:UInt8 = transitionArray[(self.opStatus.rawValue & 3) * 4 + (newValue.rawValue & 3)]
            
            if ( (trx & 1) == 1 )
            {
                self.willChangeValue(forKey: "isExecuting")
            }
            if ( (trx & 2) == 0 )
            {
                self.willChangeValue(forKey: "isFinished")
            }
            self._opStatus = newValue;
            if ( (trx & 1) == 1 )
            {
                self.didChangeValue(forKey: "isExecuting")
            }
            if ( (trx & 2) == 0 )
            {
                self.didChangeValue(forKey: "isFinished")
            }
        }
        get
        {
            return self._opStatus;
        }
    }
    

    
    convenience init!(url:URL!,tag:String) {
        
        self.init();
        
        self.opUrl = url
        self.tag = tag
    }
    
    convenience init!(urlRequest:URLRequest!,tag:String) {
        self.init()
        
        self.opRequest = urlRequest
        if let uuid = Services.shared.accountService.sessionUUID {
            opRequest.allHTTPHeaderFields = ["Content-Type" : "application/json",
                                             "Authorization": uuid]
        }else{
            opRequest.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        }
        
        self.opUrl = urlRequest.url
        self.tag = tag
    }
    
    override var isAsynchronous: Bool
        {
        return true;
    }
    
    override var isExecuting: Bool
        {
        return self.opStatus == .kBaseOperationStatus_InProgress
    }
    
    override var isReady: Bool
        {
        return true;
    }
    
    override var isFinished: Bool
        {
        return self.opStatus == .kBaseOperationStatus_Done || self.opStatus == .kBaseOperationStatis_Failed
    }

    override func start() {
        self.opStatus = .kBaseOperationStatus_InProgress;
        
        if (self.isAsynchronous)
        {
            Thread.detachNewThreadSelector(#selector(self.executeOperation), toTarget: self, with: nil);
        }
        else
        {
            self.executeOperation()
        }
    }
    
    override func cancel() {
        
        var actuallyCancel: Bool
        
        let oldCancelled = self.isCancelled;
        super.cancel()
        actuallyCancel = !oldCancelled && self.opStatus == .kBaseOperationStatus_InProgress
        
        if (actuallyCancel)
        {
            self.cancleOperation()
        }
        
    }
    
    //MARK: Base operation methods
    
    @objc func executeOperation()
    {
        if (!self.startOperation())
        {
            self.opStatus = .kBaseOperationStatis_Failed;
        }
    }
    
    func startOperation() -> Bool {

        if ( self.isCancelled )
        {
            let dict: [String:String] = [NSLocalizedDescriptionKey:"Cancelled"]
            self.opError = NSError.init(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: dict)
            self.opStatus = .kBaseOperationStatis_Failed;
            return false;
        }
        
        let urlRequest:URLRequest = self.opRequest
    
        let urlSession:URLSession = URLSession.shared
        urlSession.configuration.timeoutIntervalForRequest = 60
        urlSession.configuration.requestCachePolicy = .reloadIgnoringCacheData
        urlSession.configuration.networkServiceType = .default
        urlSession.configuration.httpShouldUsePipelining = false
        urlSession.configuration.httpShouldSetCookies = false
        urlSession.configuration.httpCookieStorage = nil
        urlSession.configuration.urlCache = nil
        
        self.opDataTask = urlSession.dataTask(with: urlRequest, completionHandler: { [weak self] (body, response,error) in
            
            if (self == nil)
            {
                return;
            }
            
            self?.actualResponseBody = body
            self?.opResponse = response as? HTTPURLResponse
        
            var err:NSError? = nil
            
            if (error != nil)
            {
                err = self?.error(code:kHTTPOperationError.kHTTPOperationErrorHTTP, description:  error!.localizedDescription)
            }
            
            if (err == nil)
            {
                if (self?.opResponse?.statusCode != 200 && self?.opResponse?.statusCode != 202)
                {
                    err = self?.error(code:kHTTPOperationError.kHTTPOperationErrorHTTP, description:  error?.localizedDescription)
                }
            }
           
            if ( err != nil )
            {
                self?.finishWithFailure(error: err)
            }
            else
            {
                self?.finishWithSuccess()
            }
            
        })
        
        self.opDataTask?.resume()
        
        return true
    }
    
    func cancleOperation() {
        
        self.opDataTask?.cancel()
        self.opDataTask = nil
        
        self.opError = NSError.init(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: nil)
        
        if ((self.opErrorBlock) != nil)
        {
            self.opErrorBlock!(self.opError,self)
        }
        self.opStatus = .kBaseOperationStatis_Failed;
    }
    
    private func error(code:kHTTPOperationError, description:String?) -> NSError
    {
        var errDict = [String:Any]()
        
        if (self.opResponse != nil)
        {
            errDict["HTTPOperationErrorHTTPResponseKey"] = self.opResponse
        }
        if ( self.actualResponseBody != nil)
        {
            errDict["HTTPOperationErrorHTTPBodyKey"] = self.actualResponseBody
        }
        if ( self.opRequest.url != nil )
        {
            errDict["HTTPOperationErrorHTTPTargetKey"] = self.opRequest.url?.absoluteString
        }
        if (description != nil)
        {
            errDict[NSLocalizedDescriptionKey] = description;
        }
        
        return NSError.init(domain: "HTTPOperationErrorDomain", code: code.rawValue, userInfo: errDict)
    }
    // MARK: Publick API methods
    
    public func finishWithSuccess()
    {
        self.opDataTask?.cancel()
        self.opDataTask = nil
        
        if ((self.opCompletionBlock) != nil)
        {
            self.opCompletionBlock!(self.actualResponseBody,self)
        }
        self.opStatus = .kBaseOperationStatus_Done
    }
    
    public func finishWithFailure(error:NSError?)
    {
        self.opDataTask?.cancel()
        self.opDataTask = nil
        
        self.opError = error
        if ((self.opErrorBlock) != nil)
        {
            self.opErrorBlock!(self.opError,self)
        }
        self.opStatus = .kBaseOperationStatis_Failed
    }
    
    public func finishWithCancel()
    {
        let dict: [String:String] = [NSLocalizedDescriptionKey:"Cancelled"]
        let error = NSError.init(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: dict)
        
        self.finishWithFailure(error: error)
    }
    
    public func setURLParamas(params:Dictionary<String,Any>)
    {
        var urlString = self.opUrl.absoluteString;
        let keys = params.keys;
        
        for key in keys
        {
            let value = params[key];
            let prefix = keys.endIndex == keys.index(of: key) ? "?" : "&"
            
            if (value != nil)
            {
                let query:String = urlString.appendingFormat("%@%@=%@", prefix,key,value as! CVarArg)
                urlString = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            }
        }
        
        self.opUrl = URL.init(string: urlString)
        self.opRequest.url = self.opUrl
    }
}



