//
//  HTTPOperationQueue.swift
//  AccountOfFinancialConsultant
//
//  Created KZN-MAC-01 on 19.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

class HTTPOperationQueue: OperationQueue
{
    public func cancelAndWaitAllOperationsFinished(cancle:Bool,completion:@escaping (()->Void))
    {
        DispatchQueue.global(qos: .default).async { [weak self] in
            
            while ( (self?.operationCount)! > 0)
            {
                if (cancle)
                {
                    self?.cancelAllOperations()
                }
                self?.waitUntilAllOperationsAreFinished();
            }
            
            completion();
        }
    }
}
