//
//  TaskService.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

let kSortTaskByDate = "SortTaskByDate"
let kSortTaskByPriority = "SortTaskByPriority"

class TaskService{
    
    var sortByDate : Bool{
        set
        {
            UserDefaults.standard.set(newValue, forKey: kSortTaskByDate)
        }
        get
        {
            return UserDefaults.standard.bool(forKey: kSortTaskByDate)
        }
    }
    
    var sortByPriority : Bool{
        set
        {
            UserDefaults.standard.set(newValue, forKey: kSortTaskByPriority)
        }
        get
        {
            return UserDefaults.standard.bool(forKey: kSortTaskByPriority)
        }
    }
    
}
