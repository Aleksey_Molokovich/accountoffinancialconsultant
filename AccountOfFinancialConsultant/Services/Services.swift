//
//  Services.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 25.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation

let kFirstEntry = "FirstEntry"

class Services{
    
    
    static let shared = Services()
    
    let accountService = AccountService()
    
    let httpOperationService = HTTPOperationQueue.init()
    
    var taskService = TaskService()
    let dao = CoreDataDAO()
    
    init() {
        if !UserDefaults.standard.bool(forKey: kFirstEntry){
            taskService.sortByPriority = true
            taskService.sortByDate = false
            UserDefaults.standard.set(true, forKey: kFirstEntry)
        }
    }

    
}
