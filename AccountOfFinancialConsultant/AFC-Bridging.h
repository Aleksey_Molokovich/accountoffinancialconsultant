//
//  CBChat-Bridging.h
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 28.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

#ifndef CBChat_Bridging_h
#define CBChat_Bridging_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "CBChatView.h"
#import "CBPrototype.h"
#import "CBMessage.h"
#import "CBMessageStorage.h"
#import <CommonCrypto/CommonCrypto.h>

#endif /* CBChat_Bridging_h */
