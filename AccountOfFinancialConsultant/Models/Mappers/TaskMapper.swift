//
//  TaskMapper.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper


class TaskMapper {
    
    class func map(from : TaskModel, to : TaskCD )
    {
        to.id = Int64(from.id)
        to.createdAt = from.createdAt
        to.updatedAt = from.updatedAt
        to.deletedAt = from.deletedAt
        to.name = from.name
        to.userLogin = from.userLogin
        to.specification = from.specification
        to.priority = Int64(from.priority)
        to.owner = from.owner
        to.status = Int64(from.status)
        to.deadlineDate = from.deadlineDate
        to.closeDate = from.closeDate
    }
    
    class func map(from : TaskCD ) -> TaskModel
    {
        
        var json : [String : Any] = [:]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        json["id"] = from.id
        
        if from.createdAt != nil {
            json["created_at"] = dateFormatter.string(from : from.createdAt!)
        }
        
        if from.updatedAt != nil {
            json["updated_at"] = dateFormatter.string(from : from.updatedAt!)
        }
        
        if from.deletedAt != nil {
            json["deleted_at"] = dateFormatter.string(from : from.deletedAt!)
        }
        
        if from.deadlineDate != nil {
            json["deadline_date"] = dateFormatter.string(from : from.deadlineDate!)
        }
        
        if from.closeDate != nil {
            json["close_date"] = dateFormatter.string(from : from.closeDate!)
        }

        json["status"] = from.status
        json["name"] = from.name
        json["user_login"] = from.userLogin
        json["description"] = from.specification
        json["priority"] = from.priority
        json["owner"] = from.owner
        
        return Mapper<TaskModel>().map(JSON: json)!
        
    }
}
