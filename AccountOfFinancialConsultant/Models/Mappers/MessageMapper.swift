//
//  MessageMapper.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 02.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import ObjectMapper

class MessageMapper   {
    
     class func map(from : MessageModel, to : MessageCD )
    {
        to.uuid = from.uuid
        to.md5 = from.hash()
        to.phone = from.phone
        to.role = from.role
        to.text = from.text
        to.status = Int64(Int(from.status))
        to.date = Double(from.date!)
        guard let chatId = from.chatId  else {  return }
        to.chatId = Int64(chatId)
    }
    
     class func map(from : MessageCD ) -> MessageModel
    {
        
        var json : [String : Any] = [:]
        json["chatId"] = from.chatId
        json["uuid"] = from.uuid
        json["phone"] = from.phone
        json["from"] = from.role
        json["text"] = from.text
        json["status"] = from.status
        json["date"] = from.date
        json["md5"] = from.md5
        return Mapper<MessageModel>().map(JSON: json)!

    }
    
}
