//
//  ReportMapper.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class ReportMapper {
    
    
    class func map(from : ReportModel, to : ReportCD )
    {
        to.id = Int64(from.id)
        to.createdAt = from.createdAt
        to.updatedAt = from.updatedAt
        to.deletedAt = from.deletedAt
        to.period = from.period
        to.periodDate = from.periodDate
        to.fpd = from.fpd
        to.spd = from.spd
        to.tpd = from.tpd
        to.reject = from.reject
        to.low_450 = from.low_450
        to.from_450_to_650 = from.from_450_to_650
        to.over_650 = from.over_650

    }
    
    class func map(from : ReportCD ) -> ReportModel
    {
        
        var json : [String : Any] = [:]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        json["id"] = from.id
        if let createdAt = from.createdAt {
            json["created_at"] = dateFormatter.string(from : createdAt)
        }
        if let updatedAt = from.updatedAt {
            json["updated_at"] = dateFormatter.string(from : updatedAt)
        }
        if let deletedAt = from.deletedAt {
            json["deleted_at"] = dateFormatter.string(from : deletedAt)
        }
        if let periodDate = from.periodDate {
            json["period_date"] = dateFormatter.string(from : periodDate)
        }
        json["period"] = from.period
        json["fpd"] = from.fpd
        json["spd"] = from.spd
        json["tpd"] = from.tpd
        json["reject"] = from.reject
        json["low_450"] = from.low_450
        json["from_450_to_650"] = from.from_450_to_650
        json["over_650"] = from.over_650
        
        return Mapper<ReportModel>().map(JSON: json)!
        
    }
    

}
