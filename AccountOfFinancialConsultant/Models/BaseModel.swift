//
//  BaseModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper

let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

class BaseModel : Mappable{
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        fatalError("Abstact method")
    }
    
    let dateTransform = TransformOf<Date, String>(fromJSON: { (value: String?) -> Date? in
        guard let _ = value else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        
        if (dateFormatter.date(from: value!) != nil){
            return dateFormatter.date(from: value!)
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX"
        
        if (dateFormatter.date(from: value!) != nil){
            return dateFormatter.date(from: value!)
        }
        
        return nil
    }, toJSON: { (value: Date?) -> String? in
        guard let _ = value else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        
        if dateFormatter.string(from: value!).count>0 {
            return dateFormatter.string(from: value!)
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX"
        if dateFormatter.string(from: value!).count>0 {
            return dateFormatter.string(from: value!)
        }
        return nil
    })
    
}

//https://github.com/Hearst-DD/ObjectMapper/issues/799
open class MyDecimalNumberTransform: TransformType {
    public typealias Object = NSDecimalNumber
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> NSDecimalNumber? {
        if let string = value as? String {
            return NSDecimalNumber(string: string)
        } else if let number = value as? NSNumber {
            let handler = NSDecimalNumberHandler(roundingMode: .plain, scale: 3, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
            return NSDecimalNumber(decimal: number.decimalValue).rounding(accordingToBehavior: handler)
        } else if let double = value as? Double {
            return NSDecimalNumber(floatLiteral: double)
        }
        return nil
    }
    
    public func transformToJSON(_ value: NSDecimalNumber?) -> String? {
        guard let value = value else { return nil }
        return value.description
    }
}
