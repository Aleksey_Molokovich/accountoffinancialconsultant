//
//  AuthModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 27.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import UIKit
import ObjectMapper

class AuthModel: Mappable {

    var uuid : String = ""
    var name : String = ""
    var surname : String = ""
    var lastname : String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        uuid        <- map["uuid"]
        name        <- map["name"]
        surname     <- map["lastname"]
        lastname    <- map["surname"]
    }
}
