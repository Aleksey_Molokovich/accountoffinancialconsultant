//
//  MessageModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 29.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageModel: BaseModel {


    
    var chatId : Int?
    var uuid : String?
    var phone : String?
    var role : String?
    var text : String = ""
//    var filpath : String?
    var status : Int = 0
    var date : Double?
    var md5 : String?
    
    
    // Mappable
    override func mapping(map: Map) {
        chatId      <- map["chatId"]
        role        <- map["from"]
        uuid        <- map["uuid"]
        status      <- map["status"]
        text        <- map["text"]
        date        <- map["date"]
        phone       <- map["phone"]
    }
    
    
    func hash() -> String {
        return (self.text + String(format:"%f", self.date!)).md5!
    }
}
