//
//  ReportModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 03.07.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper

class ReportModel : BaseModel {

    
    override func mapping(map: Map) {
        id                  <- map["id"]
        createdAt           <- (map["created_at"], dateTransform)
        updatedAt           <- (map["updated_at"], dateTransform)
        deletedAt           <- (map["deleted_at"], dateTransform)
        login               <- map["login"]
        period              <- map["period"]
        periodDate          <- (map["period_date"], dateTransform)
        fpd                 <- map["fpd"]
        spd                 <- map["spd"]
        tpd                 <- map["tpd"]
        reject              <- map["reject"]
        low_450             <- map["low_450"]
        from_450_to_650     <- map["from_450_to_650"]
        over_650            <- map["over_650"]
    }

    var id : Int = 0
    var createdAt : Date?
    var updatedAt : Date?
    var deletedAt : Date?
    var login : String?
    var period : String?
    var periodDate : Date?
    var fpd : Float = -1
    var spd : Float = -1
    var tpd : Float = -1
    var reject : Float = -1
    var low_450 : Float = 0
    var from_450_to_650 : Float = 0
    var over_650 : Float = 0
}
