//
//  TaskModel.swift
//  AccountOfFinancialConsultant
//
//  Created by KZN-MAC-01 on 21.06.2018.
//  Copyright © 2018 KZN-MAC-01. All rights reserved.
//

import Foundation
import ObjectMapper

class TaskModel : BaseModel {
    

    var id : Int = 0
    var createdAt : Date?
    var updatedAt : Date?
    var deletedAt : Date?
    var name : String?
    var userLogin : String?
    var specification : String?
    var priority : Int = 0
    var status : Int = 0
    var owner : String?
    var deadlineDate : Date?
    var closeDate : Date?
    
    
    // Mappable
    override func mapping(map: Map) {
        
        id              <- map["id"]
        createdAt       <- (map["created_at"], dateTransform)
        updatedAt       <- (map["updated_at"], dateTransform)
        deletedAt       <- (map["deleted_at"], dateTransform)
        userLogin       <- map["user_login"]
        name            <- map["name"]
        status            <- map["status"]
        specification     <- map["description"]
        priority        <- map["priority"]
        owner           <- map["owner"]
        deadlineDate    <- (map["deadline_date"], dateTransform)
        closeDate       <- (map["close_date"], dateTransform)
    }
}
